import { Component, OnInit } from '@angular/core';
import { HttpquerysService } from "../services/httpquerys.service";

@Component({
  selector: 'app-promociones',
  templateUrl: './promociones.component.html',
  styleUrls: ['./promociones.component.scss']
})
export class PromocionesComponent implements OnInit {
  oferta : any[];

  constructor(
    private _auth:  HttpquerysService
  ) { }

  ngOnInit(): void {
    this.cargarOfertas();
  }

  cargarOfertas(){
    this._auth.cargarOfertas().subscribe( result => {
      this.oferta = result.ofertas;
      console.log('oferta', result);
    }

    )};
}
