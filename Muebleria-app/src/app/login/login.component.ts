import { Component, OnInit } from '@angular/core';
import { HttpquerysService } from "../services/httpquerys.service";
import { Router } from '@angular/router'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
  email: string;
  password: string;
  mensajeError: string;
  user: any;

  constructor(
    private _auth: HttpquerysService,
    private _router: Router
  ) { }

  ngOnInit() {
    this.user = this._auth.getUser();
    console.log(this.user);
    // if (this.user) {
    //   if (this.user.puesto) {
    //     if (this.user.id_puesto = 6) {
    //       this._router.navigate(['/catalogoGerente', this.user.id])
    //     }
    //     if (this.user.id_puesto = 4) {
    //       this._router.navigate(['/pedidosMensajero', this.user.id])
    //     }
    //     if (this.user.id_puesto = 3) {
    //       this._router.navigate(['/comprasVendedor', this.user.id])
    //     }
    //     if (this.user.id_puesto = 1) {
    //       this._router.navigate(['/inventarioTaller', this.user.id])
    //     }
    //   } else {
    //     this._router.navigateByUrl('/home')
    //   }
    // }
  }

  login() {
    console.log('Soy login ts', this.email, this.password)
    this._auth.login(this.email, this.password).subscribe(result => {
      if (result.success == true) {
        if (result.empleado) {
          this._auth.setUser(result.empleado)
          console.log("GET user login", this._auth.getUser())
          console.log(result.empleado.id_puesto, 'tipo puesto')

          if (result.empleado.id_puesto == 6) {
            console.log('Gerente')
            this._router.navigate(['/catalogoGerente', result.empleado.id])
          }
          if (result.empleado.id_puesto == 4) {
            console.log('Mensajero')
            this._router.navigate(['/pedidosMensajero', result.empleado.id])
          }
          if (result.empleado.id_puesto == 5) {
            console.log('Vendedor')
            this._router.navigate(['/comprasVendedor', result.empleado.id])
          }
          if (result.empleado.id_puesto == 1) {
            console.log('Tapicero')
            this._router.navigate(['/inventarioTaller', result.empleado.id])
          }
        }
        if (result.cliente) {
          console.log(result, 'result es cliente')
          this._auth.setUser(result.cliente)
          this._router.navigateByUrl('/home')
          this.email = "";
          this.password = "";
        }
      } else {
        this.mensajeError = "Datos inválidos"
      }
    },
      error => {
        this.mensajeError = "Datos inválidos"
        console.log(this.mensajeError)
      })

  }
}
