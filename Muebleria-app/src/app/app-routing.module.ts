import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReportesComponent } from './gerente/reportes/reportes.component';
import { LoginComponent } from './login/login.component'
import { HomeComponent } from './home/home.component'
import { RegistroComponent } from './registro/registro.component'
import { DetallePComponent } from './detalle-p/detalle-p.component'
import { CatalogoGerenteComponent } from './gerente/catalogo-gerente/catalogo-gerente.component';
import { InventarioComponent } from './taller/inventario/inventario.component';
import { PedidosComponent } from './mensajero/pedidos/pedidos.component';
import { CompraComponent } from './vendedor/compra/compra.component';
import { FacturasComponent } from './vendedor/facturas/facturas.component';
import { AgregarProductoComponent } from './taller/agregar-producto/agregar-producto.component';
import { CarritoComponent } from './carrito/carrito.component';
import { PerfilClComponent } from './perfil-cl/perfil-cl.component';
import { PagoCComponent } from './pago-c/pago-c.component';
import { PromocionesComponent } from './promociones/promociones.component';
import { MiscuponesComponent } from './miscupones/miscupones.component';
import { CalificarpedidoComponent } from './calificarpedido/calificarpedido.component';
import { VentasComponent } from './gerente/ventas/ventas.component';



const routes: Routes = [
  {
    path: '',
    component: LoginComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'reports',
    component: ReportesComponent
  },
  {
    path: 'reporteVentas',
    component: VentasComponent
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'registro',
    component: RegistroComponent
  },
  {
    path: 'detalleP/:id',
    component: DetallePComponent
  },
  {
    path: 'catalogoGerente/:id',
    component: CatalogoGerenteComponent
  },
  {
    path: 'inventarioTaller/:id',
    component: InventarioComponent
  },
  {
    path: 'pedidosMensajero/:id',
    component: PedidosComponent
  },
  {
    path: 'comprasVendedor/:id',
    component: CompraComponent
  },
  {
    path: 'facturasVendedor',
    component: FacturasComponent
  },
  {
    path: 'agregarProducto',
    component: AgregarProductoComponent
  },
  {
    path: 'carrito',
    component: CarritoComponent
  },
  {
    path: 'perfilC',
    component: PerfilClComponent
  },
  {
    path: 'pago',
    component: PagoCComponent
  },
  {
    path: 'promociones',
    component: PromocionesComponent
  },
  {
    path: 'cupones',
    component: MiscuponesComponent
  },
  {
    path: 'calificarP',
    component: CalificarpedidoComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
