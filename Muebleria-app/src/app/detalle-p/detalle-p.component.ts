import { Component, OnInit,ViewChild, ElementRef, NgZone } from '@angular/core';
import { HttpquerysService } from "../services/httpquerys.service";
import { ActivatedRoute } from '@angular/router';

declare var jQuery: any;

@Component({
  selector: 'app-detalle-p',
  templateUrl: './detalle-p.component.html',
  styleUrls: ['./detalle-p.component.scss']
})
export class DetallePComponent implements OnInit {
  @ViewChild("modal", { static: false }) public modal: ElementRef;
  idProducto : any;
  productoSeleccionado : any;
  cantidad: number;
  

  constructor(
    private _auth:  HttpquerysService, 
    private _router: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.cantidad = 1;
    this.idProducto = this._router.snapshot.paramMap.get('id');
    this.cargarProducto();
  }

  cargarProducto(){
    this._auth.cargarProducto(this.idProducto).subscribe( result => {
      this.productoSeleccionado = result.producto;
      console.log('producto', this.productoSeleccionado);
    });
  }

  agregarCarrito(){
    this._auth.agregarCarrito(this.idProducto, this.cantidad);
    jQuery(this.modal.nativeElement).modal('toggle');
  }
  

}
