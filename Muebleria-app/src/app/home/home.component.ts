import { Component, OnInit } from '@angular/core';
import { HttpquerysService } from "../services/httpquerys.service";
import {Router} from '@angular/router';
import {Usuario} from '../models/usuario';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  private usuario : Usuario
  catalogo: string;
  productos: any[];
  cantidad: number[] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30]

  constructor(
    private _auth:  HttpquerysService, 
    private _router: Router
    ) { }

  ngOnInit(): void {
    console.log('User ', this._auth.getUser());
    this.cargarProductos();
  }

  cargarProductos() {
    this._auth.cargarCatalogo().subscribe( result => {
      this.productos = result.productos;
    });
      
  }

  cerrarSesion(){
    this._auth.logOut();
    this._router.navigateByUrl('/login');
  }
}
