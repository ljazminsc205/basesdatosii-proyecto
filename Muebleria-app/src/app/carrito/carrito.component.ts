import { Component, OnInit } from '@angular/core';
import { HttpquerysService } from "../services/httpquerys.service";

@Component({
  selector: 'app-carrito',
  templateUrl: './carrito.component.html',
  styleUrls: ['./carrito.component.scss']
})
export class CarritoComponent implements OnInit {
  carrito: any;
  productos: any;

  constructor(
     private _auth:  HttpquerysService
  ) { }

  ngOnInit(): void {
    this.carrito = this._auth.getCarrito();
    this.getProductosEnCarrito(this.carrito);

  }

  getProductosEnCarrito(carrito: any){
    this.productos = JSON.parse('[]');
    var i:number;
    for (i = 0; i < carrito.length; i++){
      let producto:any = carrito[i];
      this._auth.cargarProducto(producto.id_producto).subscribe( result => {
        let prodActual:any = result.producto;
        prodActual.cantidad = producto.cantidad;
        this.productos.push(prodActual);
      });

    }

  }

}
