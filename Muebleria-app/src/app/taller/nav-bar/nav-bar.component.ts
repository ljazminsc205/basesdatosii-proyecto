import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {

  nombre: string = "Taller";
  imagen: string = "https://picsum.photos/200/300";

  constructor() { }

  ngOnInit(): void {
  }

}
