import { Component, OnInit } from '@angular/core';
import { preserveWhitespacesDefault } from '@angular/compiler';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-agregar-producto',
  templateUrl: './agregar-producto.component.html',
  styleUrls: ['./agregar-producto.component.scss']
})

export class AgregarProductoComponent implements OnInit {

  producto: string;
  descripcion: string;
  precioVenta: number;
  precioSugerido: number;
  urlBase: string = 'http://15c8224b5c3b.ngrok.io/'

  constructor(private _http: HttpClient) { }

  ngOnInit(): void {
  }

  previewFile(event) {
    let preview = (<HTMLImageElement>document.getElementById('imagen'));
    let file = (<HTMLInputElement>document.getElementById('inputImg')).files[0];
    let reader = new FileReader();

    reader.addEventListener("load", function () {
      // convert image file to base64 string
      preview.src = <string>reader.result;
    }, false);

    if (file) {
      reader.readAsDataURL(file);
    }
  }

  agregarProducto() {
    let preview = (<HTMLImageElement>document.getElementById('imagen'));
    if (this.producto == undefined || this.descripcion == undefined
      || this.precioVenta == undefined || this.precioSugerido == undefined) { console.log('Datos inválidos') }
    else {
      console.log(this.producto, this.descripcion, this.precioVenta, this.precioSugerido, preview.src)
      this.postProd(preview.src).subscribe(res => {
        console.log(res)
        this.producto = '';
        this.descripcion = '';
        this.precioVenta = 0;
        this.precioSugerido = 0
      }, error => {
        console.log(error)
      })
    }
    //  / productos /: producto
  }

  postProd(foto: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*'
      })
    };
    const body = {
      "foto": foto, //string base64 de la imagen
      "nombre": this.producto,
      "precio_venta": this.precioSugerido,
      "precio_costo": this.precioVenta,
      "descripcion": this.descripcion
    }
    var urlReg = this.urlBase + 'productos/';
    return this._http.post(urlReg, body)
  }
}
