import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-inventario',
  templateUrl: './inventario.component.html',
  styleUrls: ['./inventario.component.scss']
})
export class InventarioComponent implements OnInit {

  urlBase: string = 'http://15c8224b5c3b.ngrok.io/'
  productos: any[] = [];
  product: number;
  userId: any;


  constructor(
    private _http: HttpClient,
    private _route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.userId = this._route.snapshot.paramMap.get('id');
    console.log('Id usuario: ', this.userId)
  }
  eliminarProducto(id: number) {
    console.log(id)
  }
  cargarProducto() {
    console.log(this.product)
    this.Producto().subscribe(res => {
      this.productos = []
      this.productos.push({
        id: res.producto.id, nombre: res.producto.nombre,
        foto: res.producto.foto, descripcion: res.producto.descripcion,
        precioSugerido: res.producto.precio_venta, precioVenta: res.producto.precio_costo
      })
    })
  }
  Producto() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*'
      })
    };
    var urlAuth = this.urlBase + 'productos/' + this.product
    return this._http.get<any>(urlAuth, httpOptions)
  }
  cargarTodosProductos() {
    this.Productos().subscribe(res => {
      for (let index = 0; index < res.productos.length; index++) {
        const element = res.productos[index];
        this.productos.push({
          id: element.id, nombre: element.nombre,
          foto: element.foto, descripcion: element.descripcion,
          precioSugerido: element.precio_venta, precioVenta: element.precio_costo
        })
      }
    })
  }

  Productos() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*'
      })
    };
    var urlAuth = this.urlBase + 'productos'
    return this._http.get<any>(urlAuth, httpOptions)
  }

}
