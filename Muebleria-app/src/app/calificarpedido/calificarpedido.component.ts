import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-calificarpedido',
  templateUrl: './calificarpedido.component.html',
  styleUrls: ['./calificarpedido.component.scss']
})
export class CalificarpedidoComponent implements OnInit {

  calificacion: string;
  codPedido: number;
  urlBase = 'http://15c8224b5c3b.ngrok.io/evaluaciones'
  sucursal: number;

  constructor(
    private _http: HttpClient
  ) { }

  ngOnInit(): void {
  }

  Calificar() {
    console.log(this.sucursal, this.codPedido, this.calificacion)
    this.putCal().subscribe(res => {
      console.log(res)
    })
  }
  putCal() {
    let body = {
      "id_orden": this.codPedido,
      "id_sucursal": this.sucursal,
      "evaluacion": 5 //del 1 al
    }
    return this._http.put(this.urlBase, body)
  }

  //   /evaluaciones
  // {
  //   "id_orden": int,
  //     "id_sucursal": int,
  //       "evaluacion": int //del 1 al 5
  // }




}
