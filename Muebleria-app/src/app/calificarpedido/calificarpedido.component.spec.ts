import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalificarpedidoComponent } from './calificarpedido.component';

describe('CalificarpedidoComponent', () => {
  let component: CalificarpedidoComponent;
  let fixture: ComponentFixture<CalificarpedidoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalificarpedidoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalificarpedidoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
