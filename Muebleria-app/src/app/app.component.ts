import { Component } from '@angular/core';
import { HttpquerysService } from "./services/httpquerys.service";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Muebleria-app';
  public _auth: HttpquerysService
}
