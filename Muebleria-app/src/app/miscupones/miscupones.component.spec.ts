import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MiscuponesComponent } from './miscupones.component';

describe('MiscuponesComponent', () => {
  let component: MiscuponesComponent;
  let fixture: ComponentFixture<MiscuponesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MiscuponesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MiscuponesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
