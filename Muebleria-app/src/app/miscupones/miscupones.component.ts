import { Component, OnInit } from '@angular/core';
import {Usuario} from '../models/usuario';
import { HttpquerysService } from "../services/httpquerys.service";

@Component({
  selector: 'app-miscupones',
  templateUrl: './miscupones.component.html',
  styleUrls: ['./miscupones.component.scss']
})
export class MiscuponesComponent implements OnInit {
  usuario: any;
  cupones: any[];

  constructor(
    private _auth:  HttpquerysService
  ) { }

  ngOnInit(): void {
    this.usuario = this._auth.getUser().id_cliente;
    console.log(this.usuario);
    this.mostrarCupon();
  }


  mostrarCupon(){
    this._auth.cargarCupones(this.usuario).subscribe( result => {
      this.cupones = result.cupones;
      console.log('cupon', result);
    });
  }

}
