import { Component, OnInit } from '@angular/core';
import {Usuario} from '../models/usuario';
import { HttpquerysService } from "../services/httpquerys.service";

@Component({
  selector: 'app-perfil-cl',
  templateUrl: './perfil-cl.component.html',
  styleUrls: ['./perfil-cl.component.scss']
})
export class PerfilClComponent implements OnInit {
 usuario: Usuario;

  constructor(
    private _auth:  HttpquerysService
  ) { 
  }

  ngOnInit(): void {
    this.usuario = this._auth.getUser();
    console.log('User ', this.usuario);

  }



}
