import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PerfilClComponent } from './perfil-cl.component';

describe('PerfilClComponent', () => {
  let component: PerfilClComponent;
  let fixture: ComponentFixture<PerfilClComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PerfilClComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PerfilClComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
