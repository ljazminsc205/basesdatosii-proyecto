import { Component, OnInit, ViewChild, ElementRef, NgZone} from '@angular/core';
import { HttpquerysService } from '../services/httpquerys.service';
import { Router } from '@angular/router';

declare var jQuery: any;

@Component({
  selector: 'app-pago-c',
  templateUrl: './pago-c.component.html',
  styleUrls: ['./pago-c.component.scss']
})
export class PagoCComponent implements OnInit {

   @ViewChild("modalFinalizar", { static: false }) public modalFinalizar: ElementRef;

  idCliente: any;
  carrito: any;
  idOrden: any;
  idSucursal: any;
  metodoPago: any;
  idCupon: any;
  idPromocion: any;
  productos: any;
  total: any;
  idFactura: any;

  constructor(
    private _auth:  HttpquerysService
  ) { }

  ngOnInit(): void {
    this.total = 0;
    this.idCliente = this._auth.getUser().id_cliente;
    this.carrito = this._auth.getCarrito();
    this.getProductosEnCarrito(this.carrito);
      
    console.log(this.total)
     
  }

  agregarOrden(){
    this._auth.crearOrden(this.idCliente, null, this.carrito).subscribe(result => {
          this.idOrden = result.id_orden;
          this.idSucursal = result.id_sucursal;
          this.facturarOrden();
        },
          error => { console.log(error) }
        )
  }

  facturarOrden(){
   this._auth.facturarOrden(this.idOrden, this.metodoPago, null,
      this.idCupon, this.idPromocion, this.idSucursal).subscribe(result => { 
        console.log("funciono facturar orden")
        this.borrarCarrito();
        this.idFactura = result.id_factura;
        jQuery(this.modalFinalizar.nativeElement).modal('toggle');
        },
          error => { console.log(error) }
        )}

  borrarCarrito(){
    this._auth.setCarrito(null);
  }

  getProductosEnCarrito(carrito: any){
    this.productos = JSON.parse('[]');
    var i:number;
    for (i = 0; i < carrito.length; i++){
      let producto:any = carrito[i];
      this._auth.cargarProducto(producto.id_producto).subscribe( result => {
        let prodActual:any = result.producto;
        prodActual.cantidad = producto.cantidad;
        this.productos.push(prodActual);
        this.total += prodActual.cantidad * prodActual.precio_venta;
      });
    }
  }

}
