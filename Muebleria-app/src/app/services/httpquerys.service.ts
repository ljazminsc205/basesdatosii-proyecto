import { Injectable } from '@angular/core';
import { Usuario } from '../models/usuario';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()

export class HttpquerysService {
  private usuario: any;

  private carrito: any;

  urlBase: string = 'https://15c8224b5c3b.ngrok.io/';
  // urlBase: string = 'http://localhost:1234/';


  miVar: string;

  constructor(private _http: HttpClient) { }

  login(email: string, password: string) {
    console.log('Soy login service', email, password)
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*'
      })
    };


    //var urlAuth = "http://localhost:1234/usuarios/" + email + "/" + password
    var urlAuth = this.urlBase + 'login/' + email + "/" + password
    return this._http.get<any>(urlAuth, httpOptions)
  }

  getUser() {
    if (!this.usuario) {
      this.usuario = JSON.parse(localStorage.getItem('user'));
    }
    return this.usuario;
  }

  retrieveUser(pEmail: string, pass: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*'
      })
    };
    var urlAuth = this.urlBase + 'usuarios/' + pEmail + '/' + pass
    return this._http.get<any>(urlAuth, httpOptions)
  }

  setUser(pUsuario: any) {
    this.usuario = pUsuario;
    localStorage.setItem('user', JSON.stringify(pUsuario));
  }

  register(correo: string, contrasenna: string, nombre: string, apellidos: string,
    fecha_nacimiento: Date, latitud: number, longitud: number) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*'
      })
    };
    const body = {
      "ubicacion": {
        "latitud": latitud,
        "longitud": longitud
      },
      "nombre": nombre,
      "apellido": apellidos,
      "fecha_nacimiento": fecha_nacimiento,
      "email": correo,
      "password": contrasenna

    }
    var urlReg = this.urlBase+"usuarios/clientes/" 
    return this._http.post(urlReg, body)
   // var urlReg = "https://030178345475.ngrok.io/usuarios/clientes/" + JSON.stringify(body);
    //return this._http.post(urlReg, null)

  }

  logOut() {
    this.usuario = undefined
    localStorage.removeItem('user')
  }


  cargarCatalogo() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*'
      })
    };
    var urlReg = this.urlBase + 'productos'
    return this._http.get<any>(urlReg, httpOptions)
  }

  cargarProducto(idProducto: number) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*'
      })
    };
    var urlReg = this.urlBase + 'productos/' + idProducto
    return this._http.get<any>(urlReg, httpOptions)
  }

  cargarCupones(idCliente: number) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*'
      })
    };
    var urlReg = this.urlBase + 'usuarios/clientes/' + idCliente + '/cupones'
    return this._http.get<any>(urlReg, httpOptions)
  }

  cargarOfertas() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*'
      })
    };
    var urlReg = this.urlBase + 'ofertas'
    return this._http.get<any>(urlReg, httpOptions)
  }

  
  agregarCarrito(idProducto: number, cantidad: number ){
    var carrito: any;
    if(!this.getCarrito()){
      carrito = JSON.parse('[]');
    }else{
      carrito = this.getCarrito();
    }
    carrito.push(JSON.parse('{"id_producto": '+ idProducto +', "cantidad": '+ cantidad +'}'));
    this.setCarrito(carrito);
    console.log(this.carrito);
    

  }

  getCarrito(){
    if (!this.carrito) {
      this.carrito = JSON.parse(localStorage.getItem('carrito'));
    }
     return this.carrito;

  }

  setCarrito(pcarrito: any) {
    this.carrito = pcarrito;
    localStorage.setItem('carrito', JSON.stringify(pcarrito));
  }

  //agregarCarrito(idProducto: number, cantidad: number) {
  //}

  getProductoData(idProducto: number) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*'
      })
    };
    var urlReg = this.urlBase + 'productos/' + idProducto
    return this._http.get<any>(urlReg, httpOptions)
  }

  getProducto(idProducto: number) {
    console.log(this.getProductoData(idProducto));
    return null;
  }


  
  //Hacer una orden
  crearOrden(idCliente:number, idSucursal: number, carrito: any){
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*'
      })
    };
    const body = {
      "id_cliente": idCliente,
      "id_sucursal": idSucursal, //si es null se autoasigna basado en proximidad
     "productos": carrito
    }
    var urlReg = this.urlBase + "ordenes/" 
    return this._http.post<any>(urlReg, body)
  }

  facturarOrden(id_orden: number, id_metodo_pago: number, id_vendedor: number, 
                id_cupon: number, id_oferta: number, idSucursal: number){
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*'
      })
    };
    const body = {
      "id_orden": id_orden,
      "id_metodo_pago": id_metodo_pago,
      "id_vendedor": id_vendedor,
      "id_cupon": id_cupon, //puede ser null si no se usa
      "id_oferta": id_oferta //puede ser null si no se usa
    }
    var urlReg = this.urlBase + "sucursales/" + idSucursal +'/facturas'
    return this._http.post<any>(urlReg, body)

  }



}




