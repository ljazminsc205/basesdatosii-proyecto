import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CatalogoGerenteComponent } from './catalogo-gerente.component';

describe('CatalogoGerenteComponent', () => {
  let component: CatalogoGerenteComponent;
  let fixture: ComponentFixture<CatalogoGerenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CatalogoGerenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatalogoGerenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
