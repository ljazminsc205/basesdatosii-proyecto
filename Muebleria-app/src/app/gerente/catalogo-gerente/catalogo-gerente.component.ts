
import { Component, OnInit } from '@angular/core';
import { HttpquerysService } from '../../services/httpquerys.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-catalogo-gerente',
  templateUrl: './catalogo-gerente.component.html',
  styleUrls: ['./catalogo-gerente.component.scss']
})
export class CatalogoGerenteComponent implements OnInit {
  catalogo: number;
  catalogos: any[] = []
  cantidad: number[] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30]
  urlBase: string = 'http://15c8224b5c3b.ngrok.io/'
  productos: any[] = [];
  product: number;
  userId: any;
  sucursal: number;

  cont: number;

  constructor(
    private _auth: HttpquerysService,
    private _http: HttpClient,
    private _route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.userId = this._route.snapshot.paramMap.get('id');
    console.log('Id usuario: ', this.userId)
    this.cargarSuc()
    this.getSuc(this.userId)
  }
  eliminarProducto(id: number) {
    console.log(id)
  }
  cargarSuc() {
    this.Sucursales().subscribe(res => {
      this.catalogos = res.sucursales.map((item) => {
        return { nombre: item.nombre, id: item.id }
      })
      this.catalogos.push({ nombre: 'Taller', id: -1 })
      console.log(this.catalogos, 'Cat')
    })
  }
  Sucursales() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*'
      })
    };
    var urlAuth = this.urlBase + 'sucursales'
    return this._http.get<any>(urlAuth, httpOptions)
  }
  cargarProducto() {
    console.log(this.product)
    this.Producto().subscribe(res => {
      this.productos = []
      this.productos.push({
        id: res.producto.id, nombre: res.producto.nombre,
        foto: res.producto.foto, descripcion: res.producto.descripcion,
        precioSugerido: res.producto.precio_venta, precioVenta: res.producto.precio_costo
      })
    })
  }
  Producto() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*'
      })
    };
    var urlAuth = this.urlBase + 'productos/' + this.product
    return this._http.get<any>(urlAuth, httpOptions)
  }
  cargarTodosProductos() {
    this.productos = []
    console.log(this.catalogo)
    if (this.catalogo == -1) {
      this.Productos().subscribe(res => {
        for (let index = 0; index < res.productos.length; index++) {
          const element = res.productos[index];
          this.productos.push({
            id: element.id, nombre: element.nombre,
            foto: element.foto, descripcion: element.descripcion,
            precioSugerido: element.precio_venta, precioVenta: element.precio_costo
          })
        }
      })
    }
    if (this.catalogo != -1) {
      this.ProductosSuc().subscribe(res => {
        console.log(res)
        for (let index = 0; index < res.inventario.length; index++) {
          const element = res.inventario[index];
          this.productos.push({
            id: element.id, nombre: element.nombre,
            foto: element.foto, descripcion: element.descripcion,
            precioSugerido: element.precio_venta, precioVenta: element.precio_costo, cantidad: element.cantidad
          })
          console.log(this.productos)
        }
      })
    }
  }
  ProductosSuc() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*'
      })
    };
    ///http://15c8224b5c3b.ngrok.io/sucursales/0/inventario
    var urlAuth = this.urlBase + 'sucursales/' + this.sucursal + '/inventario'
    console.log(urlAuth)
    return this._http.get<any>(urlAuth, httpOptions)
  }
  Productos() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*'
      })
    };
    var urlAuth = this.urlBase + 'productos'
    return this._http.get<any>(urlAuth, httpOptions)
  }
  getSuc(id: number) {
    this.getEmpleado(id).subscribe(res => {
      this.sucursal = res.empleado.id_sucursal
      console.log(res, 'res')
    })
  }
  getEmpleado(id: number) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*'
      })
    };
    var urlAuth = this.urlBase + 'empleados/' + id
    return this._http.get<any>(urlAuth, httpOptions)
  }
  agregarProducto(id: number) {
    console.log(id, this.cont)
    this.putProd(id).subscribe(res => {
      console.log(res)
    })
  }
  ///sucursales/:id_sucursal/inventario
  putProd(id: number) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*'
      })
    };
    const body = {
      "id_producto": id,
      "cantidad": +this.cont
    }
    var urlReg = this.urlBase + 'sucursales/' + this.sucursal + '/inventario';
    return this._http.post(urlReg, body)
  }



}
