import { Component, OnInit } from '@angular/core';
import { HttpquerysService } from '../../services/httpquerys.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-ventas',
  templateUrl: './ventas.component.html',
  styleUrls: ['./ventas.component.scss']
})
export class VentasComponent implements OnInit {

  urlBase: string = 'http://15c8224b5c3b.ngrok.io/'
  sucursales: any[];
  sucursal: number;
  productos: any[] = [];
  producto: number;
  vendedores: string[] = [];
  vendedor: number;
  fechaIn: string = 'NULL';
  fechaFin: string = 'NULL';
  user: any;
  userId: any;
  reporte: any[] = [];


  constructor(
    private _auth: HttpquerysService,
    private _http: HttpClient,
    private _route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    console.log("Reportes::ngOnInit ", this._auth.miVar)
    console.log(this.cargarSuc())
    console.log(this.cargarProd())
    console.log(this.cargarVen())
    this.userId = this._route.snapshot.paramMap.get('id');
    console.log('Id usuario: ', this.userId)
  }


  generarReporte() {
    //console.log(this.sucursal, this.producto, this.vendedor, this.fechaIn, this.fechaFin)
    let body: string = 'http://15c8224b5c3b.ngrok.io/reportes/ventas?';
    if (this.sucursal != undefined) {
      if (body.length < 46) {
        body = body + 'id_sucursal=' + this.sucursal
      }
      else {
        body = body + '&' + 'id_sucursal=' + this.sucursal
      }
    }
    if (this.producto != undefined) {
      if (body.length < 46) {
        body = body + 'id_producto=' + this.producto
      }
      else {
        body = body + '&' + 'id_producto=' + this.producto
      }
    }
    if (this.vendedor != undefined) {
      if (body.length < 46) {
        body = body + 'id_vendedor=' + this.vendedor
      }
      else {
        body = body + '&' + 'id_vendedor=' + this.vendedor
      }
    }
    if (this.fechaIn != 'NULL') {
      if (body.length < 46) {
        body = body + 'fecha_inicio=' + this.fechaIn
      }
      else {
        body = body + '&' + 'fecha_inicio=' + this.fechaIn
      }
    }
    if (this.fechaFin != 'NULL') {

      if (body.length < 46) {
        body = body + 'fecha_final=' + this.fechaFin
      }
      else {
        body = body + '&' + 'fecha_final=' + this.fechaFin
      }
    }
    this.getReporte(body).subscribe(res => {
      console.log(res.reporte)
      this.reporte = res.reporte.map((item) => {
        return { sucursal: item.sucursal, producto: item.producto, cantidad: item.cantidad, vendedor: item.vendedor, ventas: item.venta }
      })
    })
    this.sucursal = undefined;
    this.vendedor = undefined;
    this.producto = undefined;
    this.fechaIn = 'NULL';
    this.fechaFin = 'NULL';
  }
  getReporte(body: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*'
      })
    };
    return this._http.get<any>(body, httpOptions)
  }

  cargarVen() {
    this.Vendedor().subscribe(res => {
      console.log(res.empleados, 'vendedores')
      this.vendedores = res.empleados.map((item) => {
        return { nombre: item.nombre, apellido: item.apellido, id: item.id }
      })
    })
  }
  Vendedor() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*'
      })
    };
    var urlAuth = this.urlBase + 'vendedores'
    return this._http.get<any>(urlAuth, httpOptions)
  }
  cargarSuc() {
    this.Sucursales().subscribe(res => {
      console.log(res.sucursales, 'sucursales')
      this.sucursales = res.sucursales.map((item) => {
        return { nombre: item.nombre, id: item.id }
      })
    })
  }
  Sucursales() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*'
      })
    };
    var urlAuth = this.urlBase + 'sucursales'
    return this._http.get<any>(urlAuth, httpOptions)
  }
  cargarProd() {
    this.Productos().subscribe(res => {
      console.log(res.productos, 'prod')
      for (let index = 0; index < res.productos.length; index++) {
        const element = res.productos[index];
        this.productos.push({ nombre: element.nombre, id: element.id })
      }
    })
  }
  Productos() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*'
      })
    };
    var urlAuth = this.urlBase + 'productos'
    return this._http.get<any>(urlAuth, httpOptions)
  }


}
