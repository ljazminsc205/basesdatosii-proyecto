import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nav-bar-gerente',
  templateUrl: './nav-bar-gerente.component.html',
  styleUrls: ['./nav-bar-gerente.component.scss']
})
export class NavBarGerenteComponent implements OnInit {
  nombre: string = "Gerente";
  imagen: string = "https://picsum.photos/200/300";

  constructor() { }

  ngOnInit(): void {
  }

}
