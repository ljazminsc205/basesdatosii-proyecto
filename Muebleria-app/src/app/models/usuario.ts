export interface Usuario {
    id_cliente: number
    nombre: string,
    apellido: string,
    correo: string,
    fecha_nacimiento: Date,
    latitud: number,
    longitud: number   
}
