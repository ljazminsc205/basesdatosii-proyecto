import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReportesComponent } from './gerente/reportes/reportes.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { RegistroComponent } from './registro/registro.component';
import { DetallePComponent } from './detalle-p/detalle-p.component';
import { CarritoComponent } from './carrito/carrito.component';
import { PerfilClComponent } from './perfil-cl/perfil-cl.component';
import { PagoCComponent } from './pago-c/pago-c.component';
import { NavBarGerenteComponent } from './gerente/nav-bar-gerente/nav-bar-gerente.component';
import { FormsModule } from '@angular/forms';
import { CatalogoGerenteComponent } from './gerente/catalogo-gerente/catalogo-gerente.component';
import { InventarioComponent } from './taller/inventario/inventario.component';
import { NavBarComponent } from './taller/nav-bar/nav-bar.component';
import { PedidosComponent } from './mensajero/pedidos/pedidos.component';
import { CompraComponent } from './vendedor/compra/compra.component';
import { NavvBarMensajeroComponent } from './mensajero/navv-bar-mensajero/navv-bar-mensajero.component';
import { NavBarVendedorComponent } from './vendedor/nav-bar-vendedor/nav-bar-vendedor.component';
import { FacturasComponent } from './vendedor/facturas/facturas.component';
import { AgregarProductoComponent } from './taller/agregar-producto/agregar-producto.component';
import { GoogleMapsModule } from '@angular/google-maps';
import { HttpClientModule } from '@angular/common/http';
import { MiscuponesComponent } from './miscupones/miscupones.component';
import { PromocionesComponent } from './promociones/promociones.component';
import { CalificarpedidoComponent } from './calificarpedido/calificarpedido.component'
import { HttpquerysService } from './services/httpquerys.service';
import { VentasComponent } from './gerente/ventas/ventas.component';



@NgModule({
  declarations: [
    AppComponent,
    ReportesComponent,
    LoginComponent,
    HomeComponent,
    RegistroComponent,
    DetallePComponent,
    CarritoComponent,
    PerfilClComponent,
    PagoCComponent,
    NavBarGerenteComponent,
    CatalogoGerenteComponent,
    InventarioComponent,
    NavBarComponent,
    PedidosComponent,
    CompraComponent,
    NavvBarMensajeroComponent,
    NavBarVendedorComponent,
    FacturasComponent,
    AgregarProductoComponent,
    MiscuponesComponent,
    PromocionesComponent,
    CalificarpedidoComponent,
    VentasComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    GoogleMapsModule,
    HttpClientModule
  ],
  providers: [HttpquerysService],
  bootstrap: [AppComponent]
})
export class AppModule { }
