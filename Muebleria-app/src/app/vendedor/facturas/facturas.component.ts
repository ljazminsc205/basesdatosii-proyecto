import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-facturas',
  templateUrl: './facturas.component.html',
  styleUrls: ['./facturas.component.scss']
})
export class FacturasComponent implements OnInit {
  facturas: any[]
  idFactura: number;
  factura: any[]
  sucursal: number;


  constructor() { }

  ngOnInit(): void {
    this.facturas = [
      {
        idFactura: 1,
        fecha: "20/05/2020",
        monto: 35000
      },
      {
        idFactura: 1,
        fecha: "20/05/2020",
        monto: 20000
      }
    ]
    this.factura = [
      {
        producto: 'Silla',
        precio: 2600,
        descripcion: 'silla roja',
        cantidad: 2
      },
      {
        producto: 'Silla',
        precio: 2600,
        descripcion: 'silla roja',
        cantidad: 2
      },
      {
        producto: 'Silla',
        precio: 2600,
        descripcion: 'silla roja',
        cantidad: 2
      }
    ]
  }

  verDetalles(idFactura) {
    console.log(idFactura)
  }
}
