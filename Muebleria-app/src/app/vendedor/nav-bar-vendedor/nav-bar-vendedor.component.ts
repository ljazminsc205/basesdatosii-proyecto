import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-nav-bar-vendedor',
  templateUrl: './nav-bar-vendedor.component.html',
  styleUrls: ['./nav-bar-vendedor.component.scss']
})
export class NavBarVendedorComponent implements OnInit {

  @Input() nombre: string;
  @Input() imagen: string;


  constructor() { }

  ngOnInit(): void {
  }

}
