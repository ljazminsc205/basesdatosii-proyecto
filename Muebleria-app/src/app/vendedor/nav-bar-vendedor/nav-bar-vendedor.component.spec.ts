import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavBarVendedorComponent } from './nav-bar-vendedor.component';

describe('NavBarVendedorComponent', () => {
  let component: NavBarVendedorComponent;
  let fixture: ComponentFixture<NavBarVendedorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavBarVendedorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavBarVendedorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
