import { Component, OnInit, ɵConsole } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-compra',
  templateUrl: './compra.component.html',
  styleUrls: ['./compra.component.scss']
})
export class CompraComponent implements OnInit {
  productos: any[] = [];
  productosFiltrados: any[] = [];
  product: string = '';
  cliente: any = "sin asignar";
  pedido: any[] = [];
  cantidad: number[] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30]
  cant: number;
  idCupon: number;
  idPromocion: number;
  userId: any;
  urlBase = 'http://15c8224b5c3b.ngrok.io/'
  sucursal: any;
  metodoPagoSeleccionado: any;
  subtotal: number = 0;
  total: number = 0;
  nombreEmpleado: string;
  imagenEmpleado: string;

  metodosPago: any[] = [];
  ofertas: any[] = [];
  cupones: any[] = [];

  constructor(
    private _route: ActivatedRoute,
    private _http: HttpClient
  ) { }

  ngOnInit(): void {
    this.userId = this._route.snapshot.paramMap.get('id');
    console.log('Id usuario: ', this.userId)
    this.getSuc(this.userId);
    this.getOfertas().subscribe(res => {
      this.ofertas = res.ofertas;
      console.log("Ofertas: ", this.ofertas)
    })
    this.getCupones().subscribe(res => {
      this.cupones = res.cupones;
      console.log("Cupones: ", this.cupones)
    })
  }

  getSuc(id: number) {
    this.getEmpleado(id).subscribe(res => {
      console.log("Empleado: ", res.empleado)
      this.sucursal = res.empleado.id_sucursal
      this.nombreEmpleado = res.empleado.nombre;
      this.imagenEmpleado = res.empleado.foto;
      console.log("Sucursal: ", this.sucursal)
      //Buscar productos
      this.getProductos().subscribe(res => {
        this.productos = res.inventario;
        this.productosFiltrados = this.productos;
      })
      this.getMetodosPago().subscribe(res => {
        console.log("getMetodosPago::res", res);
        this.metodosPago = res.metodos;
        console.log("Metodos pago: ", this.metodosPago);
      })
    })
  }
  getEmpleado(id: number) {
    var urlAuth = this.urlBase + 'empleados/' + id
    return this._http.get<any>(urlAuth)
  }

  getProductos() {
    let url = `${this.urlBase}sucursales/${this.sucursal}/inventario`
    return this._http.get<any>(url);
  }

  getOfertas() {
    let url = this.urlBase + "ofertas"
    return this._http.get<any>(url)
  }
  getCupones() {
    let url = this.urlBase + "cupones"
    return this._http.get<any>(url)
  }

  getMetodosPago() {
    let url = `${this.urlBase}sucursales/${this.sucursal}/metodos`;
    console.log("URL metodos: ", url)
    return this._http.get<any>(url);
  }

  verificarCupon() {
    console.log(this.idCupon)
  }
  verificarPromocion() {
    console.log(this.idCupon)
  }
  cargarProductos() {
    console.log(this.product)
    if (this.product != "") {
      this.productosFiltrados = this.productos.filter(p => {
        if (p.id == this.product) {
          return p;
        }
      })
    } else {
      this.productosFiltrados = this.productos;
    }
  }
  verificarCliente(id) {
    this.getCliente(id).subscribe(res => {
      console.log(res)
      if (res.success == false) {
        // indicar error
      } else {
        this.cliente = res.cliente;
        console.log("Cliente: ", this.cliente)
      }
    })
  }
  getCliente(id) {
    let url = `${this.urlBase}usuarios/clientes/${id}`;
    return this._http.get<any>(url);
  }
  hacerPedido(producto: any, cantidad: number) {
    // console.log("Producto: ", producto);
    // console.log("Cantidad: ", cantidad);
    if (cantidad == 0) {
      return;
    }
    producto.cantidadCompra = +cantidad;
    this.subtotal += producto.precio_venta * cantidad;
    this.pedido.push(producto)
    this.productos.forEach(p => {
      if (p.id == producto.id) {
        p.cantidad -= cantidad;
      }
    });
    console.log("Productos carrito: ", this.pedido)
  }
  facturar() {
    console.log(this.pedido)
    this.total = this.subtotal;
  }
  modificarPrecio(tipo: string) {
    if (tipo == "promocion") {
      this.ofertas.forEach(oferta => {
        if (oferta.id == this.idPromocion) {
          this.total = this.total - this.total * oferta.descuento_porcentual * 0.01
        }
      });
    } else {
      this.cupones.forEach(cupon => {
        if (cupon.id == this.idCupon) {
          this.total = this.total - this.total * cupon.descuento_porcentual * 0.01
        }
      });
    }
  }
  pagar() {
    let url = this.urlBase + "ordenes";
    let productosOrden = this.pedido.map(item => {
      return { id_producto: item.id, cantidad: item.cantidadCompra };
    })
    let order = {
      id_cliente: this.cliente.id,
      id_sucursal: this.sucursal,
      productos: productosOrden
    }
    this._http.post<any>(url, order).subscribe(res => {
      if (res.success == true) {
        let orden_id = res.id_orden;

        let metodoPagoOrden = (this.metodoPagoSeleccionado != undefined) ? this.metodoPagoSeleccionado : null;
        let cuponOrden = (this.idCupon != undefined) ? this.idCupon : null;
        let promoOrden = (this.idPromocion != undefined) ? this.idPromocion : null;
        let facturarBody = {
          id_orden: orden_id,
          id_vendedor: this.userId,
          id_metodo_pago: metodoPagoOrden,
          id_cupon: cuponOrden,
          id_oferta: promoOrden
        }
        let urlFacturar = `${this.urlBase}sucursales/${this.sucursal}/facturas`;
        this._http.post<any>(url, facturarBody).subscribe(res => {
          console.log("Facturado");
        });
      }
    })
  }

}
