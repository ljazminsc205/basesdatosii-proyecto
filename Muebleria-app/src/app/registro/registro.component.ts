import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { MapMarker, GoogleMap } from '@angular/google-maps';
import { HttpquerysService } from '../services/httpquerys.service'
import { Router } from '@angular/router'

declare var jQuery: any;



@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.scss']
})
export class RegistroComponent implements OnInit {
  @ViewChild("modalMap", { static: false }) public modalMap: ElementRef;


  @ViewChild("googleMap", { static: false }) public googleMap: GoogleMap;
  onDragStart() {
    console.log(this.googleMap.getClickableIcons());
  }

  onDragEnd() {
    console.log(JSON.stringify(this.googleMap.getCenter()));
  }
  //Datos para registrar
  nombreUsuario: string;
  apellidos: string;
  email: string;
  password: string;
  fecha_nacimiento: Date;

  lat: number;
  long: number;

  mensaje: string;

  //Datos para extraer la ubicacion
  markerOptions = { draggable: true };
  markerPositions: google.maps.LatLngLiteral[] = [];
  latitude: number;
  longitude: number;
  zoom: number;
  address: string;
  private geoCoder;
  display?: google.maps.LatLngLiteral;
  center = { lat: 9.85, lng: -83.91 };
  options: google.maps.MapOptions = {
    mapTypeId: 'hybrid',
    zoomControl: true,
    scrollwheel: false,
    disableDoubleClickZoom: true,
    maxZoom: 18,
    minZoom: 8,
  }



  @ViewChild("mapMarker", { static: false }) public mapMarker: MapMarker;
  public marker =
    {
      position: { lat: 9.85300331, lng: -83.90540580179746 },
      label: {
        color: 'red'
      },
      title: 'Posicion actual',
      options: { animation: google.maps.Animation.DROP, draggable: false },
      clickable: false
    };
  onMouseOver() {
    //console.log(this.mapMarker.getVisible);
  }

  mapDragend() {
    console.log(JSON.stringify(this.googleMap.getCenter()));
  }

  move() {
    //this.display = event.latLng.toJSON();
  }

  setMarker() {
    this.mapMarker.position = JSON.parse(JSON.stringify(this.googleMap.getCenter()));
    this.marker.position = JSON.parse(JSON.stringify(this.googleMap.getCenter()));
  }


  constructor(
    private _router: Router,
    private _auth: HttpquerysService

  ) { }

  ngOnInit(): void {
    this.setCurrentLocation();
  }

  private setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.googleMap.center = { lat: this.latitude, lng: this.longitude };
        this.zoom = 16;
        //this.mapMarker.setPosition = {lat: this.latitude, lng: this.longitude};
        //this.mapMarker.setLabel = { color: 'red' };
        //this.mapMarker.setTitle = 'Marker';
        this.marker.position = { lat: this.latitude, lng: this.longitude };
      });
    }
  }

  markerDragEnd($event: MouseEvent) {
    console.log($event);
    //this.latitude = $event.coords.lat;
    //this.longitude = $event.coords.lng;
    this.getAddress(this.latitude, this.longitude);
  }

  getAddress(latitude, longitude) {
    this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, (results, status) => {
      console.log(results);
      console.log(status);
      if (status === 'OK') {
        if (results[0]) {
          this.zoom = 12;
          this.address = results[0].formatted_address;
          window.alert('Ubicacion seleccioada: Lat = ' + this.latitude + ', Lon = ' + this.longitude);
        } else {
          window.alert('No results found');
        }
      } else {
        window.alert('Geocoder failed due to: ' + status);
      }

    });
  }

  obtenerDireccion() {
    this.lat = this.marker.position.lat;
    this.long = this.marker.position.lng;
    jQuery(this.modalMap.nativeElement).modal('toggle');
  }

  validarDatos() {
    if ((this.nombreUsuario != undefined) && (this.apellidos != undefined) && (this.email != undefined) &&
      (this.password != undefined) && (this.fecha_nacimiento != undefined) && (this.lat != undefined)
      && (this.long != undefined)) {
      return true
    } else {
      return false
    }
  }

  register() {
    if (this.validarDatos()) {
      this._auth.register(this.email, this.password, this.nombreUsuario, this.apellidos,
        this.fecha_nacimiento, this.lat, this.long).subscribe(result => {
          this.nombreUsuario = "";
          this.apellidos = "";
          this.email = "";
          this.password = "";
          this.fecha_nacimiento = undefined;
          this.lat = undefined;
          this.long = undefined;
          this._router.navigateByUrl('/login')
        },
          error => { console.log(error) }
        )
    }
    else {
      this.mensaje = "Datos invalidos"
    }
  }

}
