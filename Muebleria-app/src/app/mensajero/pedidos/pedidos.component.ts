import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-pedidos',
  templateUrl: './pedidos.component.html',
  styleUrls: ['./pedidos.component.scss']
})
export class PedidosComponent implements OnInit {

  pedidosCompletados: any[] = [];
  pedidosPendientes: any[] = [];
  pedidosTodos: any[] = [];
  clienteSeleccionado: string
  contacto: number;
  direccion: string;
  pedido: any[] = [];
  userId: any;
  sucursal: any;
  urlBase = 'http://15c8224b5c3b.ngrok.io/'

  nombreEmpleado: string;
  imagenEmpleado: string;

  constructor(private _http: HttpClient,
    private _route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.userId = this._route.snapshot.paramMap.get('id');
    console.log('Id usuario: ', this.userId)
    this.getSuc(this.userId);
    // console.log(this.getPedidosCom())
    // console.log(this.getPedidosPen())
  }
  getSuc(id: number) {
    this.getEmpleado(id).subscribe(res => {
      this.sucursal = res.empleado.id_sucursal
      this.nombreEmpleado = res.empleado.nombre;
      this.imagenEmpleado = res.empleado.foto;
      this.getPedidosCom();
      this.getPedidosPen();
      console.log("Sucursal: ", this.sucursal)
    })
  }
  getEmpleado(id: number) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*'
      })
    };
    var urlAuth = this.urlBase + 'empleados/' + id
    return this._http.get<any>(urlAuth, httpOptions)
  }
  getPedidosCom() {
    this.PedidoComp().subscribe(res => {
      console.log(res)
      for (let index = 0; index < res.ordenes.length; index++) {
        this.pedidosCompletados.push(
          { id: res.ordenes[index].id, nombreCliente: res.ordenes[index].nombre_cliente, productos: res.ordenes[index].productos, estado: true })
      }
      console.log("Completados: ", this.pedidosCompletados);
    })
  }
  PedidoComp() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*'
      })
    };
    var urlAuth = this.urlBase + 'sucursales/' + this.sucursal + '/empleados/' + this.userId + '/completadas'
    return this._http.get<any>(urlAuth, httpOptions)
  }
  getPedidosPen() {
    this.PedidoPend().subscribe(res => {
      console.log("getPedidosPen: ", res)
      for (let index = 0; index < res.ordenes.length; index++) {
        this.pedidosPendientes.push(
          { id: res.ordenes[index].id, nombreCliente: res.ordenes[index].nombre_cliente, productos: res.ordenes[index].productos, estado: false })
      }
      console.log("Pendientes: ", this.pedidosPendientes);
    })
  }
  PedidoPend() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*'
      })
    };
    var urlAuth = this.urlBase + 'sucursales/' + this.sucursal + '/empleados/' + this.userId + '/pendientes'
    return this._http.get<any>(urlAuth, httpOptions)
  }

  mostrarInfoComp(pedido: any) {
    console.log('idPedido mostrarInfo', pedido)
    this.clienteSeleccionado = pedido.cliente
    for (let index = 0; index < pedido.productos.length; index++) {

      this.pedido.push(
        {
          producto: pedido.productos[index].nombre,
          descripcion: pedido.productos[index].descripcion,
          cantidad: pedido.productos[index].cantidad
        }
      )
    }
    console.log(pedido.productos, 'soy mostrar info')
  }

  mostrarInfoPen(pedido: any) {
    console.log('idPedido mostrarInfo', pedido)
    this.clienteSeleccionado = pedido.cliente
    for (let index = 0; index < pedido.productos.length; index++) {
      this.pedido.push(
        {
          producto: pedido.productos[index].nombre,
          descripcion: pedido.productos[index].descripcion,
          cantidad: pedido.productos[index].cantidad
        }
      )
    }
    console.log(pedido.productos, 'soy mostrar info')
  }

  cambiarEstadoACompletados(pedidoId: any) {
    // this.putPedido(pedido).subscribe(res => {
    //   console.log(res)
    // })
    this.pedidosPendientes.forEach(ped => {
      if (ped.id === pedidoId) {
        this.pedidosCompletados.push(ped);
      }
    });
    this.pedidosPendientes = this.pedidosPendientes.filter(ped => {
      if (ped.id !== pedidoId) {
        return ped;
      }
    })
    this.putPedido(pedidoId).subscribe(res => {
      console.log("put pedido:", res)
    })
  }
  cambiarEstadoAPendientes(pedidoId: any) {
    // this.putPedido(pedido).subscribe(res => {
    //   console.log(res)
    // })
    console.log("Cambiando de completado a pendiente")
    this.pedidosCompletados.forEach(ped => {
      if (ped.id === pedidoId) {
        this.pedidosPendientes.push(ped);
      }
    });
    this.pedidosCompletados = this.pedidosCompletados.filter(ped => {
      if (ped.id !== pedidoId) {
        return ped;
      }
    })

  }

  putPedido(pedido: any) {
    // var urlReg = this.urlBase + 'usuarios/empleados/' + this.userId + '/ordenes/' + pedido + 'productos/';
    //var urlReg = `${this.urlBase}/sucursales/${this.sucursal}/entregas`;
    var urlReg = `${this.urlBase}sucursales/${this.sucursal}/entregas`;
    console.log("ruta put pedido: ", urlReg)
    let body = {
      id_mensajero: this.userId,
      id_orden: pedido
    }
    return this._http.put(urlReg, body)
  }
  // /usuarios/empleados/:id_mensajero/ordenes/:id_orden

}
