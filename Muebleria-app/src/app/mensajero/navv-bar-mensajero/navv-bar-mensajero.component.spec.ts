import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavvBarMensajeroComponent } from './navv-bar-mensajero.component';

describe('NavvBarMensajeroComponent', () => {
  let component: NavvBarMensajeroComponent;
  let fixture: ComponentFixture<NavvBarMensajeroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavvBarMensajeroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavvBarMensajeroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
