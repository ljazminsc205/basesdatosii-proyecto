import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-navv-bar-mensajero',
  templateUrl: './navv-bar-mensajero.component.html',
  styleUrls: ['./navv-bar-mensajero.component.scss']
})
export class NavvBarMensajeroComponent implements OnInit {

  @Input() nombre: string;
  @Input() imagen: string;

  constructor() { }

  ngOnInit(): void {
  }

}
