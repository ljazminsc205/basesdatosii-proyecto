//lib imports
const express = require('express');
const cors = require('cors');

//wrapper imports
const env = require('./wrappers/env.js');
const docs = require('./wrappers/docs.js');
const mssql = require('./wrappers/mssql.js');
const mysql = require('./wrappers/mysql.js');

//controller imports
const hrController = require('./controllers/human_resources.js');
const csController = require('./controllers/customer_support.js');
const wsController = require('./controllers/workshop.js');
const bController = require('./controllers/branch.js');

//initialize environment variables
const routes = env.server.routes;
const port = env.server.PORT;

//general purpose functions
async function createSuccessfulResponse(name = null, data = null){
    if(data){
        response = { "success": true };
        response[name] = data;
        return response;
    }
    else
        return { "success": true };
}

async function createUnsuccessfulResponse(error){
    console.error(error);
    return { "success": false, "error": error };
}

//express setup
const app = express();
app.use(cors());
app.use(express.json({ limit: '50mb', strict: 'true' }));

//Login usuario
app.get(routes.LOGIN, (request, response) => {
    const email = request.params.email;
    const password = request.params.contrasena;
    hrController.login(email, password)
        .then(user => {
            const tipo_usuario = user.tipo_usuario;
            delete user.tipo_usuario;
            if(tipo_usuario == "Cliente")
                return createSuccessfulResponse("cliente", user);
            else
                return createSuccessfulResponse("empleado", user);
        })
        .catch(error => { return createUnsuccessfulResponse(error); })
        .then(json => response.send(json));
});

//Ver documentación
app.get(routes.API_DOC, (request, response) => {
    docs.render(routes.API_DOC).then( html => {
        response.sendFile(html);
    }).catch( error => {
        createUnsuccessfulResponse(error).then(json => {
            response.send(json);
        });
    });
});

//Agregar un cliente
app.post(routes.CLIENTS, (request, response) => {
    const client = request.body;
    csController.addClient(client)
        .then(() => { return createSuccessfulResponse(); })
        .catch(error => { return createUnsuccessfulResponse(error); })
        .then(json => response.send(json));
});

//Conseguir un cliente
app.get(routes.CLIENT, (request, response) => {
    const client_id = request.params.id_cliente;
    csController.getClient(client_id)
        .then(client => { return createSuccessfulResponse("cliente", client); })
        .catch(error => { return createUnsuccessfulResponse(error); })
        .then(json => response.send(json));
});

//Modificar información de un cliente
app.put(routes.CLIENT, (request, response) => {
    const client_id = request.params.id_cliente;
    const client = request.body;
    csController.updateClient(client_id, client)
        .then(() => { return createSuccessfulResponse(); })
        .catch(error => { return createUnsuccessfulResponse(error); })
        .then(json => response.send(json));
});

//Conseguir los tipos de teléfono
app.get(routes.PHONE_TYPES, (request, response) => {
    csController.getPhoneTypes()
        .then(phoneTypes => { return createSuccessfulResponse("tipos", phoneTypes); })
        .catch(error => { return createUnsuccessfulResponse(error); })
        .then(json => response.send(json));
});

//Agregar un teléfono a una cuenta de cliente
app.post(routes.CLIENT_PHONES, (request, response) => {
    const client_id = request.params.id_cliente;
    const phone = request.body;
    csController.addPhone(client_id, phone)
        .then(() => { return createSuccessfulResponse(); })
        .catch(error => { return createUnsuccessfulResponse(error); })
        .then(json => response.send(json));
});

//Conseguir los teléfonos de un cliente
app.get(routes.CLIENT_PHONES, (request, response) => {
    const client_id = request.params.id_cliente;
    csController.getPhonesByClient(client_id)
        .then(phones => { return createSuccessfulResponse("telefonos", phones); })
        .catch(error => { return createUnsuccessfulResponse(error); })
        .then(json => response.send(json));
});

//Conseguir ofertas
app.get(routes.OFFERS, (request, response) => {
    csController.getOffers()
        .then(offers => { return createSuccessfulResponse("ofertas", offers); })
        .catch(error => { return createUnsuccessfulResponse(error); })
        .then(json => response.send(json));
});

//Conseguir cupones
app.get(routes.COUPONS, (request, response) => {
    csController.getCoupons()
        .then(coupons => { return createSuccessfulResponse("cupones", coupons); })
        .catch(error => { return createUnsuccessfulResponse(error); })
        .then(json => response.send(json));
});

//Conseguir cupones de un usuario
app.get(routes.USER_COUPONS, (request, response) => {
    const client_id = request.params.id_cliente;
    csController.getClientCoupons(client_id)
        .then(coupons => { return createSuccessfulResponse("cupones", coupons); })
        .catch(error => { return createUnsuccessfulResponse(error); })
        .then(json => response.send(json));
});

//Asignar un cupón a un cliente
app.post(routes.USER_COUPONS, (request, response) => {
    const client_id = request.params.id_cliente;
    const coupon_id = request.body.id_cupon;
    const coupon_expiration = request.body.fecha_vencimiento;
    csController.assignCoupon(coupon_id, client_id, coupon_expiration)
        .then(() => { return createSuccessfulResponse(); })
        .catch(error => { return createUnsuccessfulResponse(error); })
        .then(json => response.send(json));
});

//Conseguir sucursales
app.get(routes.BRANCHES, (request, response) => {
    bController.getBranches()
        .then(branches => { return createSuccessfulResponse("sucursales", branches); })
        .catch(error => { return createUnsuccessfulResponse(error); })
        .then(json => response.send(json));
});

//Hacer una orden
app.post(routes.ORDERS, (request, response) => {
    const order = request.body;
    bController.addOrder(order)
        .then(order_data => { return { success: true, id_sucursal: order_data.id_sucursal, id_orden: order_data.id_orden }; })
        .catch(error => { return createUnsuccessfulResponse(error); })
        .then(json => response.send(json));
});

//Conseguir una orden
app.get(routes.BRANCH_ORDER, (request, response) => {
    const branch_id = request.params.id_sucursal;
    const order_id = request.params.id_orden;
    bController.getOrder(branch_id, order_id)
        .then(order => { return createSuccessfulResponse("orden", order); })
        .catch(error => { return createUnsuccessfulResponse(error); })
        .then(json => response.send(json));
});

//Evaluar una orden
app.post(routes.EVALUATIONS, (request, response) => {
    const eval = request.body;
    bController.addOrderEval(eval.id_sucursal, eval.id_orden, eval.evaluacion)
        .then(() => { return createSuccessfulResponse(); })
        .catch(error => { return createUnsuccessfulResponse(error); })
        .then(json => response.send(json));
});

//Asignar orden a mensajero para entregar
app.post(routes.BRANCH_DELIVERIES, (request, response) => {
    const branch_id = request.params.id_sucursal;
    const courier_id = request.body.id_mensajero;
    const order_id = request.body.id_orden;
    bController.setOrderCourier(branch_id, courier_id, order_id)
        .then(() => { return createSuccessfulResponse(); })
        .catch(error => { return createUnsuccessfulResponse(error); })
        .then(json => response.send(json));
});

//Completar entrega de mensajero
app.put(routes.BRANCH_DELIVERIES, (request, response) => {
    const branch_id = request.params.id_sucursal;
    const courier_id = request.body.id_mensajero;
    const order_id = request.body.id_orden;
    bController.deliverOrder(branch_id, courier_id, order_id)
        .then(() => { return createSuccessfulResponse(); })
        .catch(error => { return createUnsuccessfulResponse(error); })
        .then(json => response.send(json));
});

//Conseguir entregas pendientes
app.get(routes.COURIER_DELIVERIES_DUE, (request, response) => {
    const branch_id = request.params.id_sucursal;
    const courier_id = request.params.id_mensajero;
    bController.getDueDeliveries(branch_id, courier_id)
        .then(orders => { return createSuccessfulResponse("ordenes", orders); })
        .catch(error => { return createUnsuccessfulResponse(error); })
        .then(json => response.send(json));
});

//Conseguir entregas completadas
app.get(routes.COURIER_DELIVERIES_DONE, (request, response) => {
    const branch_id = request.params.id_sucursal;
    const courier_id = request.params.id_mensajero;
    bController.getDoneDeliveries(branch_id, courier_id)
        .then(orders => { return createSuccessfulResponse("ordenes", orders); })
        .catch(error => { return createUnsuccessfulResponse(error); })
        .then(json => response.send(json));
});

//Conseguir métodos de pago
app.get(routes.BRANCH_PAYMENT_METHODS, (request, response) => {
    const branch_id = request.params.id_sucursal;
    bController.getPaymentMethods(branch_id)
        .then(methods => { return createSuccessfulResponse("metodos", methods); })
        .catch(error => { return createUnsuccessfulResponse(error); })
        .then(json => response.send(json));
});

//Facturar una orden
app.post(routes.RECEIPTS, (request, response) => {
    const branch_id = request.params.id_sucursal;
    const receipt = request.body;
    bController.payOrder(branch_id, receipt)
        .then(receipt_id => { return createSuccessfulResponse("id_factura", receipt_id); })
        .catch(error => { return createUnsuccessfulResponse(error); })
        .then(json => response.send(json));
});

//Conseguir una factura
app.get(routes.RECEIPT, (request, response) => {
    const branch_id = request.params.id_sucursal;
    const receipt_id = request.params.id_factura;
    bController.getReceipt(branch_id, receipt_id)
        .then(receipt => { return createSuccessfulResponse("factura", receipt); })
        .catch(error => { return createUnsuccessfulResponse(error); })
        .then(json => response.send(json));
});

//Conseguir los productos disponibles
app.get(routes.PRODUCTS, (request, response) => {
    wsController.getProducts()
        .then(products => { return createSuccessfulResponse("productos", products); })
        .catch(error => { return createUnsuccessfulResponse(error); })
        .then(json => response.send(json));
});

///Conseguir un producto
app.get(routes.PRODUCT, (request, response) => {
    const product_id = request.params.id_producto
    wsController.getProduct(product_id)
        .then(product => { return createSuccessfulResponse("producto", product); })
        .catch(error => { return createUnsuccessfulResponse(error); })
        .then(json => response.send(json));
});

//Agregar un producto
app.post(routes.PRODUCTS, (request, response) => {
    const product = request.body;
    wsController.addProduct(product)
        .then(() => { return createSuccessfulResponse(); })
        .catch(error => { return createUnsuccessfulResponse(error); })
        .then(json => response.send(json));
});

//Modificar un producto
app.put(routes.PRODUCT, (request, response) => {
    const product_id = request.params.id_producto;
    const product = request.body;
    wsController.updateProduct(product_id, product)
        .then(() => { return createSuccessfulResponse(); })
        .catch(error => { return createUnsuccessfulResponse(error); })
        .then(json => response.send(json));
});

//Conseguir inventario completo
app.get(routes.STOCK, (request, response) => {
    const branch_id = request.params.id_sucursal;
    bController.getStock(branch_id)
        .then(stock => { return createSuccessfulResponse("inventario", stock); })
        .catch(error => { return createUnsuccessfulResponse(error); })
        .then(json => response.send(json));
});

//Conseguir inventario de un producto
app.get(routes.STOCK_PRODUCT, (request, response) => {
    const branch_id = request.params.id_sucursal;
    const product_id = request.params.id_producto;
    bController.getStockForProduct(branch_id, product_id)
        .then(product => { return createSuccessfulResponse("producto", product); })
        .catch(error => { return createUnsuccessfulResponse(error); })
        .then(json => response.send(json));
});

//Agregar inventario
app.post(routes.STOCK, (request, response) => {
    const branch_id = request.params.id_sucursal;
    const stock = request.body;
    bController.addStock(branch_id, stock)
        .then(() => { return createSuccessfulResponse(); })
        .catch(error => { return createUnsuccessfulResponse(error); })
        .then(json => response.send(json));
});

//Reducir inventario
app.delete(routes.STOCK, (request, response) => {
    const branch_id = request.params.id_sucursal;
    const stock = request.body;
    bController.removeStock(branch_id, stock)
        .then(() => { return createSuccessfulResponse(); })
        .catch(error => { return createUnsuccessfulResponse(error); })
        .then(json => response.send(json));
});

//Conseguir todos los empleados
app.get(routes.EMPLOYEES, (request, response) => {
    hrController.getEmployees()
        .then(employees => { return createSuccessfulResponse("empleados", employees); })
        .catch(error => { return createUnsuccessfulResponse(error); })
        .then(json => response.send(json));
});

//Conseguir todos los vendedores
app.get(routes.SELLER_EMPLOYEES, (request, response) => {
    hrController.getSellerEmployees()
        .then(employees => { return createSuccessfulResponse("empleados", employees); })
        .catch(error => { return createUnsuccessfulResponse(error); })
        .then(json => response.send(json));
});

//Conseguir un empleado
app.get(routes.EMPLOYEE, (request, response) => {
    const employee_id = request.params.id_empleado;
    hrController.getEmployee(employee_id)
        .then(employee => { return createSuccessfulResponse("empleado", employee); })
        .catch(error => { return createUnsuccessfulResponse(error); })
        .then(json => response.send(json));
});

//Agregar un empleado
app.post(routes.EMPLOYEES, (request, response) => {
    const employee = request.body;
    hrController.addEmployee(employee)
        .then(() => { return createSuccessfulResponse(); })
        .catch(error => { return createUnsuccessfulResponse(error); })
        .then(json => response.send(json));
});

//Modificar un empleado
app.put(routes.EMPLOYEE, (request, response) => {
    const employee_id = request.params.id_empleado;
    const employee = request.body;
    hrController.updateEmployee(employee_id, employee)
        .then(() => { return createSuccessfulResponse(); })
        .catch(error => { return createUnsuccessfulResponse(error); })
        .then(json => response.send(json));
});

//Generar reporte de ventas
app.get(routes.SALES_REPORT, (request, response) => {
    const branch_id = request.query.id_sucursal;
    const product_id = request.query.id_producto;
    const seller_id = request.query.id_vendedor;
    const start_date = request.query.fecha_inicio;
    const end_date = request.query.fecha_final;
    bController.getSalesReport(branch_id, product_id, seller_id, start_date, end_date)
        .then(report => { return createSuccessfulResponse("reporte", report); })
        .catch(error => { return createUnsuccessfulResponse(error); })
        .then(json => response.send(json));
});

//Generar reporte de ganancias
app.get(routes.EARNINGS_REPORT, (request, response) => {
    const branch_id = request.query.id_sucursal;
    const product_id = request.query.id_producto;
    const seller_id = request.query.id_vendedor;
    const start_date = request.query.fecha_inicio;
    const end_date = request.query.fecha_final;
    bController.getEarningsReport(branch_id, product_id, seller_id, start_date, end_date)
        .then(report => { return createSuccessfulResponse("reporte", report); })
        .catch(error => { return createUnsuccessfulResponse(error); })
        .then(json => response.send(json));
});

//any other route
app.all(routes.ANY, (request, response) => {
    createUnsuccessfulResponse("UNHANDLED_ROUTE").then((json) => {
        response.send(json);
    });
});

mssql.connect()
    .then(() => {
        console.log('Connected to MSSQL.');
        return mysql.connect();
    })
    .then(() => {
        console.log('Connected to MySQL.');
        app.listen(port, () => console.log("Listening on port " + port + "..."));
    })
    .catch((error) => {
        console.error(error);
    });