const mssql = require('../wrappers/mssql.js');
const DB = 'RecursosHumanos';
const SELLER_ID = 5; //the database id for the seller role

//logs in any user
async function login(email, password){
    const request = await mssql.request(DB);
    request.input('email', mssql.string, email);
    request.input('pass', mssql.string, password);
    const response = await request.execute('sp_login');
    const user = response.recordsets[0][0];
    if(user.tipo_usuario == 'Cliente'){
        user.ubicacion = { latitud: user.latitud, longitud: user.longitud };
        delete user.latitud;
        delete user.longitud;
    }
    return user; //first record set, first record
}

function formatEmployee(employee){
    employee.puesto = { id: employee.id_puesto, nombre: employee.puesto };
    delete employee.id_puesto;
    return employee;
}

async function getEmployees(){
    const request = await mssql.request(DB);
    const response = await request.execute('buscarEmpleado');
    const employees = response.recordsets[0];
    employees.forEach(employee => { formatEmployee(employee); });
    return employees;
}

async function getSellerEmployees(){
    const request = await mssql.request(DB);
    request.input('idPuesto', mssql.integer, SELLER_ID);
    const response = await request.execute('buscarEmpleado');
    const employees = response.recordsets[0];
    employees.forEach(employee => { formatEmployee(employee); });
    return employees;
}

async function getEmployee(employee_id){
    const request = await mssql.request(DB);
    request.input('idEmpleado', mssql.integer, employee_id);
    const response = await request.execute('buscarEmpleado');
    const employee = response.recordsets[0][0];
    formatEmployee(employee);
    return employee;
}

async function addEmployee(employee){
    const request = await mssql.request(DB);
    request.input('sucursal', mssql.integer, employee.id_sucursal);
    request.input('puesto', mssql.integer, employee.id_puesto);
    request.input('foto', mssql.string, employee.foto);
    request.input('nombre', mssql.string, employee.nombre);
    request.input('apellido', mssql.string, employee.apellido);
    request.input('telefono', mssql.string, employee.telefono);
    request.input('correo', mssql.string, employee.email);
    request.input('pass', mssql.string, employee.password);
    request.input('cedula', mssql.string, employee.cedula);
    await request.execute('agregarEmpleadoSucursal');
}

async function updateEmployee(employee_id, employee){
    const request = await mssql.request(DB);
    request.input('id', mssql.integer, employee_id);
    request.input('foto', mssql.string, employee.foto);
    request.input('puesto', mssql.integer, employee.id_puesto);
    request.input('salario', mssql.decimal(20,2), employee.salario);
    request.input('telefono', mssql.string, employee.telefono);
    request.input('correo', mssql.string, employee.email);
    request.input('pass', mssql.string, employee.password);
    request.input('nombre', mssql.string, employee.nombre);
    request.input('apellido', mssql.string, employee.apellido);
    request.input('cedula', mssql.string, employee.cedula);
    await request.execute('updateEmpleado');
}

module.exports = {
    login,
    getEmployees,
    getSellerEmployees,
    getEmployee,
    addEmployee,
    updateEmployee
}