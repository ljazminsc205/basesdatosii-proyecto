const mysql = require('../wrappers/mysql.js');
const DB = 'AtencionCliente';

//receives a client JSON as data and adds it to the DB
async function addClient(client){
    const query = 'CALL sp_AgregarCliente(' + 
        mysql.querify(client.ubicacion.latitud, false) + ', ' +
        mysql.querify(client.ubicacion.longitud, false) + ', ' +
        mysql.querify(client.nombre) + ', ' +
        mysql.querify(client.apellido) + ', ' + 
        mysql.querify(client.fecha_nacimiento) + ', ' + 
        mysql.querify(client.email) + ', ' + 
        mysql.querify(client.password) + ");";
    return await mysql.query(DB, query);
}

function formatClient(client){
    console.log(client);
    const latitud = client.ubicacion.x;
    const longitud = client.ubicacion.y;
    client.ubicacion = { latitud, longitud };
}

//returns the phone types
async function getClient(client_id){
    const response = await mysql.query(DB, 'CALL sp_BuscarCliente(' + mysql.querify(client_id, false) +');');
    formatClient(response[0]);
    return response[0];
}

//receives a client JSON as data and updates the client at the id
async function updateClient(client_id, client){

    //if the spatial data is not added then add nulls as dummies
    if(client.ubicacion == null)
        client.ubicacion = {
            latitud: null,
            longitud: null
        }

    const query = 'CALL sp_ModificarCliente(' + 
        client_id + ', ' +    
        mysql.querify(client.ubicacion.latitud, false) + ', ' +
        mysql.querify(client.ubicacion.longitud, false) + ', ' +
        mysql.querify(client.nombre) + ', ' +
        mysql.querify(client.apellido) + ', ' + 
        mysql.querify(client.fecha_nacimiento) + ', ' + 
        mysql.querify(client.email) + ', ' + 
        mysql.querify(client.password) + ");";
    return await mysql.query(DB, query);
}

//returns the phone types
async function getPhoneTypes(){
    return await mysql.query(DB, 'CALL sp_BuscarTipoTelefono();');
}

//adds a phone and binds it to a client
async function addPhone(client_id, phone){
    const query = 'CALL sp_AgregarTelefono(' + 
        mysql.querify(client_id, false) + ', ' +
        mysql.querify(phone.id_tipo, false) + ', ' + 
        mysql.querify(phone.numero) + ');';
    return await mysql.query(DB, query);
}

//gets all the phones for a client
async function getPhonesByClient(client_id){
    const query = 'CALL sp_BuscarTelefonoPorCliente(' + mysql.querify(client_id, false) + ');';
    return await mysql.query(DB, query);
}

//gets all the available offers
async function getOffers(){
    const query = 'CALL sp_BuscarOferta(null);';
    return await mysql.query(DB, query);
}

//gets all the available coupons
async function getCoupons(){
    const query = 'CALL sp_BuscarCupon(null);';
    return await mysql.query(DB, query);
}

//gets coupons available for a particular client
async function getClientCoupons(client_id){
    const query = 'CALL sp_BuscarCuponPorCliente(' + mysql.querify(client_id, false) + ');';
    return await mysql.query(DB, query);
}

//gives a coupon to a client
async function assignCoupon(coupon_id, client_id, expiry_date){
    const query = 'CALL sp_AsignarCuponACliente(' + 
        mysql.querify(coupon_id, false) + ', ' +
        mysql.querify(client_id, false) + ', ' + 
        mysql.querify(expiry_date) + ');';
    return await mysql.query(DB, query);
}

module.exports = {
    addClient,
    getClient,
    formatClient,
    updateClient,
    getPhoneTypes,
    addPhone,
    getPhonesByClient,
    getOffers,
    getCoupons,
    getClientCoupons,
    assignCoupon
}