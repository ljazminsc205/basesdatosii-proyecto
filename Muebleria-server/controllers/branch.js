const mssql = require('../wrappers/mssql.js');


async function getBranches(){
    const request = await mssql.request('RecursosHumanos');
    const response = await request.execute('verSucursales');
    const branches = [];
    response.recordsets[0].forEach(element => {
        branches.push({
            id: element.id,
            nombre: element.nombre,
            nombre_base: element.nombre_base,
            ubicacion: {
                latitud: element.ubicacion.points[0].x,
                longitud: element.ubicacion.points[0].y
            }
        });
    });
    return branches;
}

//Database names are obtained from a table in the HR DB
async function getBranchDBName(branch_id){
    const branches = await getBranches();
    let db_name = null;
    branches.forEach(branch => {
        if(branch.id == branch_id)
            db_name = branch.nombre_base;
    });
    if(db_name != null)
        return db_name;
    else
        throw "getBranchDBName: Sucursal de id = "+branch_id+" no encontrada.";        
}

async function addOrder(order){
    let request = null;
    let response = null;
    //if the branch id is not included, then look it up
    if(order.id_sucursal == null){
        request = await mssql.request('RecursosHumanos');
        request.input('idCliente', mssql.integer, order.id_cliente);
        response = await request.execute('buscar_sucursal_cercana');
        order.id_sucursal = response.recordsets[0][0].id;
    }

    //get the branch db name
    const db = await getBranchDBName(order.id_sucursal);
    
    //add the order to the branch
    request = await mssql.request(db);
    request.input('idCliente', mssql.integer, order.id_cliente);
    request.input('idSucursal', mssql.integer, order.id_sucursal);
    response = await request.execute('crearOrden');

    //get the order id from the response
    const order_id = response.recordsets[0][0].id;

    for(let i = 0; i < order.productos.length; i++){
        request = await mssql.request(db);
        request.input('orden', mssql.integer, order_id);
        request.input('prod', mssql.integer, order.productos[i].id_producto);
        request.input('cantidad', mssql.integer, order.productos[i].cantidad);
        await request.execute('agregarDetalle');
    };

    //returns the order id and branch id that was just inserted
    return { id_orden: response.recordsets[0][0].id, id_sucursal: response.recordsets[0][0].id_sucursal };
}

async function getOrder(branch_id, order_id){
    //get the branch db name
    const db = await getBranchDBName(branch_id);

    //get the order
    let request = await mssql.request(db);
    request.input('idOrden', mssql.integer, order_id);
    let response = await request.execute('verOrden');
    const order = response.recordsets[0][0];

    //get the order details
    request = await mssql.request(db);
    request.input('idOrden', mssql.integer, order_id);
    response = await request.execute('verDetalleOrden');
    order.productos = response.recordsets[0];

    order.productos.forEach(producto => { delete producto.id_orden; });

    return order;
}

async function addOrderEval(branch_id, order_id, score){
    //get the branch db name
    const db = await getBranchDBName(branch_id);

    const request = await mssql.request(db);
    request.input('orden', mssql.integer, order_id);
    request.input('evaluacion', mssql.integer, score);
    await request.execute('agregarEvaluacion');
}

async function getPaymentMethods(branch_id){
    //get the branch db name
    const db = await getBranchDBName(branch_id);

    const request = await mssql.request(db);
    const response = await request.execute('getMetodoPago');
    return response.recordsets[0];
}

async function setOrderCourier(branch_id, courier_id, order_id){
    //get the branch db name
    const db = await getBranchDBName(branch_id);

    const request = await mssql.request(db);
    request.input('orden', mssql.integer, order_id);
    request.input('mensajero', mssql.integer, courier_id);
    await request.execute('agregarPedidoMensajero');
}

async function deliverOrder(branch_id, courier_id, order_id){
    //get the branch db name
    const db = await getBranchDBName(branch_id);

    const request = await mssql.request(db);
    request.input('idPedido', mssql.integer, order_id);
    request.input('estado', mssql.bit, 1);
    await request.execute('actualizarPedido');
}

async function getDeliveries(branch_id, courier_id, proc_name){
    //get the branch db name
    const db = await getBranchDBName(branch_id);

    //getting order metadata
    let request = await mssql.request(db);
    request.input('idMensajero', mssql.integer, courier_id);
    let response = await request.execute(proc_name);
    const orders = response.recordsets[0];

    //getting order products
    for(let i = 0; i < orders.length; i++){
        request = await mssql.request(db);
        request.input('idPedido', mssql.integer, orders[i].id);
        response = await request.execute('verPedido');
        orders[i].productos = response.recordsets[0];
    }
    return orders;
}

async function getDueDeliveries(branch_id, courier_id){
    return await getDeliveries(branch_id, courier_id, 'verPedidosPendientes');
}

async function getDoneDeliveries(branch_id, courier_id){
    return await getDeliveries(branch_id, courier_id, 'verPedidosEntregados');
}

async function payOrder(branch_id, receipt){
    //get the branch db name
    const db = await getBranchDBName(branch_id);

    const request = await mssql.request(db);
    request.input('idOrden', mssql.integer, receipt.id_orden);
    request.input('tipoPago', mssql.integer, receipt.id_metodo_pago);
    request.input('idVendedor', mssql.integer, receipt.id_vendedor);
    request.input('idCupon', mssql.integer, receipt.id_cupon);
    request.input('idOferta', mssql.integer, receipt.id_oferta);
    const response = await request.execute('facturar');

    //returns the receipt id that was just inserted
    return response.recordsets[0][0].id;
}

async function getReceipt(branch_id, receipt_id){
    //get the branch db name
    const db = await getBranchDBName(branch_id);

    const request = await mssql.request(db);
    request.input('id', mssql.integer, receipt_id);
    const response = await request.execute('getFactura');
    const receipt = response.recordsets[0][0];
    
    //format the receipt properly
    if(receipt.id_vendedor != null){
        receipt.vendedor = { id: receipt.id_vendedor, nombre: receipt.nombre_vendedor };
        delete receipt.id_vendedor;
        delete receipt.nombre_vendedor;
    }
    receipt.cliente = { id: receipt.id_cliente, nombre: receipt.nombre_cliente };
    delete receipt.id_cliente;
    delete receipt.nombre_cliente;
    receipt.metodo_pago = { id: receipt.id_metodo_pago, nombre: receipt.nombre_metodo_pago };
    delete receipt.id_metodo_pago;
    delete receipt.nombre_metodo_pago;

    return receipt;
}

async function formatStockProduct(product){
    product.sucursal = { id: product.sucursal, nombre: product.nombre_sucursal };
    delete product.nombre_sucursal;
}

async function getStock(branch_id){
    //get the branch db name
    const db = await getBranchDBName(branch_id);

    const request = await mssql.request(db);
    const response = await request.execute('verInventario');
    response.recordsets[0].forEach(product => { formatStockProduct(product); });
    return response.recordsets[0];
}

async function getStockForProduct(branch_id, product_id){
    //get the branch db name
    const db = await getBranchDBName(branch_id);

    const request = await mssql.request(db);
    request.input('id', mssql.integer, product_id);
    const response = await request.execute('buscarProd');
    formatStockProduct(response.recordsets[0][0]);
    return response.recordsets[0][0];
}

async function addStock(branch_id, stock){
    //get the branch db name
    const db = await getBranchDBName(branch_id);

    const request = await mssql.request(db);
    request.input('idProd', mssql.integer, stock.id_producto);
    request.input('idSucursal', mssql.integer, branch_id);
    request.input('cantidad', mssql.integer, stock.cantidad);
    await request.execute('agregarProd');
}

async function removeStock(branch_id, stock){
    //get the branch db name
    const db = await getBranchDBName(branch_id);

    const request = await mssql.request(db);
    request.input('id', mssql.integer, stock.id_producto);
    request.input('cantidad', mssql.integer, stock.cantidad);
    await request.execute('reducirProd');
}

async function getSalesReport(branch_id, product_id, seller_id, start_date, end_date){
    const request = await mssql.request();
    request.input('sucursal', mssql.integer, branch_id);
    request.input('prod', mssql.integer, product_id);
    request.input('vendedor', mssql.integer, seller_id);
    request.input('fechaI', mssql.date, start_date);
    request.input('fechaF', mssql.date, end_date);
    const response = await request.execute('sp_ventas');
    return response.recordsets[0];
}

async function getEarningsReport(branch_id, product_id, seller_id, start_date, end_date){
    const request = await mssql.request();
    request.input('sucursal', mssql.integer, branch_id);
    request.input('prod', mssql.integer, product_id);
    request.input('vendedor', mssql.integer, seller_id);
    request.input('fechaI', mssql.date, start_date);
    request.input('fechaF', mssql.date, end_date);
    const response = await request.execute('sp_ganancias');
    return response.recordsets[0];
}

module.exports = {
    getBranches,
    addOrder,
    getOrder,
    addOrderEval,
    getPaymentMethods,
    setOrderCourier,
    deliverOrder,
    getDueDeliveries,
    getDoneDeliveries,
    payOrder,
    getReceipt,
    getStock,
    getStockForProduct,
    addStock,
    removeStock,
    getSalesReport,
    getEarningsReport
}