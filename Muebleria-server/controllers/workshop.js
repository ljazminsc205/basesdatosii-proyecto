const mssql = require('../wrappers/mssql.js');
const DB = 'Taller';

async function getProducts(){
    const request = await mssql.request(DB);
    const response = await request.execute('verProductos');
    return response.recordsets[0];
}

async function getProduct(product_id){
    const request = await mssql.request(DB);
    request.input('idProd', mssql.string, product_id);
    const response = await request.execute('verProd');
    return response.recordsets[0][0];
}

async function addProduct(product){
    const request = await mssql.request(DB);
    request.input('imgProd', mssql.string, product.foto);
    request.input('nombre', mssql.string, product.nombre);
    request.input('precioVenta', mssql.decimal(20,2), product.precio_venta);
    request.input('precioCosto', mssql.decimal(20,2), product.precio_costo);
    request.input('descripcion', mssql.string, product.descripcion);
    await request.execute('agregarProducto');
}

async function updateProduct(product_id, product){
    const request = await mssql.request(DB);
    request.input('id', mssql.integer, product_id);
    request.input('foto', mssql.string, product.foto);
    request.input('nombre', mssql.string, product.nombre);
    request.input('pventa', mssql.decimal(20,2), product.precio_venta);
    request.input('pcosto', mssql.decimal(20,2), product.precio_costo);
    request.input('desc', mssql.string, product.descripcion);
    await request.execute('updateProd');
}

module.exports = {
    getProducts,
    getProduct,
    addProduct,
    updateProduct
}