const fs = require('fs');
const dotenv = require('dotenv');

dotenv.config();

//server REST routes
const routes = JSON.parse(fs.readFileSync('routes.json'));

//server variables
const server = {
    PORT: process.env.SERVER_PORT,
    routes
};

//microsoft sql server variables
const mssql = {
    USER: process.env.MSSQL_USER,
    PASSWORD: process.env.MSSQL_PASSWORD,
    SERVER: process.env.MSSQL_SERVER,
    DEFAULT_DB: process.env.MSSQL_DB_DEFAULT
}

//mysql variables
const mysql = {
    HOST: process.env.MYSQL_HOST,
    PORT: process.env.MYSQL_PORT,
    USER: process.env.MYSQL_USER,
    PASSWORD: process.env.MYSQL_PASSWORD
}

console.log('Loaded environment variables.');

module.exports = {
    server,
    mssql,
    mysql
};