const sql = require('mssql');
const env = require('./env.js');

const config = {
    server: env.mssql.SERVER,
    user: env.mssql.USER,
    password: env.mssql.PASSWORD,
    database: env.mssql.DEFAULT_DB,
    options: {
        enableArithAbort: true
    }
};

async function connect(){
    await sql.connect(config);
}

//returns a request on a particular db
async function request(database = config.database) {
    const pool = await sql.connect(config);
    await pool.query('USE ['+database+'];');
    return pool.request();
}

module.exports = {
    connect,
    request,
    bit: sql.Bit,
    integer: sql.Int,
    decimal: sql.Decimal,
    string: sql.NVarChar,
    boolean: sql.Bit,
    date: sql.DateTime
}