const mysql = require('promise-mysql');
const env = require('./env.js');
const { json } = require('express');

let pool = null;

const config = {
    host: env.mysql.HOST,
    user: env.mysql.USER,
    password: env.mysql.PASSWORD,
    port: env.mssql.PORT
};

async function connect(){
    pool = await mysql.createPool(config);
    const connection = await pool.getConnection();
    await connection.release();
}

//queries a particular db
async function query(database, queryString) {
    try{
        const connection = await pool.getConnection();
        await connection.query('USE `'+database+'`;');
        const response = await connection.query(queryString);
        await connection.release();
        return response[0];
    } catch(error){
        throw error.sqlMessage;
    }
}

//returns a query ready version of a value
//note that both undefined and null resolve to 'null'
function querify(value, quote = true){
    if(value === undefined || value === null)
        return 'null';
    else if(quote)
        return '\''+ value + '\'';
    else
        return '' + value;
}

module.exports = {
    connect,
    query,
    querify
}