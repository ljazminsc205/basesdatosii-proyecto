const Renderer = require('docsify-server-renderer');
const readFileSync = require('fs').readFileSync;

const renderer = new Renderer({
    template: readFileSync('./docs/index.html', 'utf-8')
});

async function render(url){
    console.log(url);
    try {
        return await renderer.renderToString('docs/');
    } catch(error) {
        console.log(error);
        throw "DOC_RENDER_ERROR";
    }
}

module.exports = {
    render
}