## **Bienvenido**

Esta es la documentación del API para un proyecto desarrollado en el curso **Bases de Datos II** en **ITCR** durante el primer semestre del 2020. Describe todas las solicitudes HTTP posibles en el sistema, separadas por casos de uso. Cada descripción de solicitud incluye el método HTTP, los datos JSON requeridos, la ruta y el JSON devuelto.

---
## **Guía**

A continuación se muestra qué se incluye en cada método HTTP listado. Las palabras **método**, **solicitud** o **ruta** pueden usarse intercambiablemente en este documento, y todas se refieren a los métodos HTTP que se enumeran a continuación. Así es como se verá cada uno de ellos:

> ### Nombre del Método {docsify-ignore}
> ##### `HTTP-VERB` /path/to/resource/:parameter/:query? {docsify-ignore}
>
> Descripción del método.
>
> **Recibe:**
>```json
>{
>   "required": string,
>   "JSON": boolean,
>   "object": number
>}
>```
> **Devuelve:**
>```json
>{
>   "returned": number,
>   "JSON": array,
>   "object": string
>}
>```

Algunas notas importantes sobre este formato:
* Algunas secciones pueden omitirse si no son necesarias.
* `HTTP-VERB` sería algo como `GET`,` POST`, `PUT` o` DELETE`.
* El **:parameter** en la URL es un **comodín**. Esto significa que puede aceptar cualquier string. Por lo general, se utiliza para poner identificadores de recursos.
* El **:query?** en la URL es una **consulta**. Puede encontrar más información sobre consultas HTTP [aquí](https://en.wikipedia.org/wiki/Query_string).
* La sección **Receibe** especifica un objeto JSON que debe enviarse en la solicitud a través de su cuerpo.
* La sección **Devuelve** especifica un objeto JSON que se recibirá de las solicitudes si no se producen errores.

### Manejo de errores {docsify-ignore}

Siempre que el servidor devuelva un resultado habrá un campo `success`. Este campo determina si hubo o no un error en la transacción. Si hubo un error este será `false`, sino será `true`. Los clientes siempre deben verificar este campo en busca de errores y presentar un error cuando se establece en falso. Los errores además contienen un campo de `error` con el string de error a mostrar el la interfaz. Los errores se ven así:

```json
{
    "success": false,
    "error": string
}
```

---
## **Métodos HTTP**

Todos los métodos aquí listados son accesibles mediante el API.

### Ver documentación
##### `GET` /docs {docsify-ignore}

Devuelve esta página con la documentación del API.

`NO FUNCIONA ACTUALEMNTE`

### Login de usuario
##### `GET` /login/:email/:contrasena {docsify-ignore}

Hace login del cliente o empleado. Lo que devuelve depende de si el usuario es un empleado o un cliente.

**Devuelve:**
```json
{
    "success": true,
    "cliente":{
        "id": int,
        "ubicacion":{
            "latitud": number,
            "longitud": number
        },
        "nombre": string,
        "apellido": string,
        "fecha_nacimiento": date, //'YYYY-MM-DD'
        "email": string
    }
}
```

ó

```json
{
    "success": true,
    "empleado":{
        "id": int,
        "id_sucursal":int,
        "id_puesto": int,
        "foto": string, //string base64 con la foto
        "nombre": string,
        "apellido": string,
        "salario": number,
        "telefono": string,
        "email": string,
        "fecha_contratacion": date, //'YYYY-MM-DD'
        "cedula": string
    }
}
```

### Conseguir sucursales
##### `GET` /sucursales {docsify-ignore}

Devuelve todas las sucursales registradas en la base de datos de Recursos Humanos.

**Devuelve:**
```json
{
    "success": true,
    "sucursales":[
        {
            "id": int,
            "nombre": string,
            "nombre_base": string,
            "ubicacion": {
                "latitud": number,
                "longitud": number
            }
        },
        ...n sucursales
    ]
}
```

---
## **Clientes**

### Conseguir un cliente
##### `GET` /usuarios/clientes/:id_cliente {docsify-ignore}

Recibe la información para crear una cuenta de cliente.

**Devuelve:**
```json
{
    "success": true,
    "cliente":{
        "id": int,
        "nombre": string,
        "apellido": string,
        "fecha_nacimiento": date, //'YYYY-MM-DD'
        "email": string,
        "ubicacion":{
            "latitud": number,
            "longitud": number
        }
    }
}
```

### Agregar un cliente
##### `POST` /usuarios/clientes {docsify-ignore}

Recibe la información para crear una cuenta de cliente.

**Recibe:**
```json
{
    "ubicacion":{
        "latitud": number,
        "longitud": number
    },
    "nombre": string,
    "apellido": string,
    "fecha_nacimiento": date, //'YYYY-MM-DD'
    "email": string,
    "password": string
}
```
**Devuelve:**
```json
{
    "success": true
}
```

### Modificar información de un cliente
##### `PUT` /usuarios/clientes/:id_cliente {docsify-ignore}

Recibe la información para crear una cuenta de cliente.

**Recibe:**
```json
{
    "ubicacion":{
        "latitud": number,
        "longitud": number
    },
    "nombre": string,
    "apellido": string,
    "fecha_nacimiento": date, //'YYYY-MM-DD'
    "email": string,
    "password": string
}
```
**Devuelve:**
```json
{
    "success": true
}
```

### Conseguir los tipos de teléfono
##### `GET` /telefonos/tipos {docsify-ignore}

Devuelve los tipos de teléfono disponibles en el sistema.

**Devuelve:**
```json
{
    "success": true,
    "tipos": [ 
        {  
            "id": int,
            "nombre": string
        },
        ...n tipos
    ]
}
```

### Agregar un teléfono a un cliente
##### `POST` /usuarios/clientes/:id_cliente/telefonos {docsify-ignore}

Recibe un nuevo número telefónico para un cliente.

**Recibe:**
```json
{
    "id_tipo": int,
    "numero": string //debe ser de 8 digitos
}
```
**Devuelve:**
```json
{
    "success": true
}
```

### Conseguir los teléfonos de un cliente
##### `GET` /usuarios/clientes/:id_cliente/telefonos {docsify-ignore}

Devuelve los números de teléfono para un usuario.

**Devuelve:**
```json
{
    "success": true,
    "telefonos":[
        {
            "id": int,
            "id_cliente": int,
            "id_tipo": int,
            "numero": int
        },
        ...n telefonos
    ]
}
```

### Conseguir ofertas
##### `GET` /ofertas {docsify-ignore}

Devuelve todos las ofertas disponibles y vigentes en el sistema. Ofertas vencidas no se devuelven.

**Devuelve:**
```json
{
    "success": true,
    "ofertas":[
        {
            "id": int,
            "descuento_porcentual": number,
            "descripcion": string,
            "fecha_emision": date, //'YYYY-MM-DD'
            "fecha_vencimiento": date //'YYYY-MM-DD'
        },
        ...n cupones
    ]
}
```

### Conseguir cupones
##### `GET` /cupones {docsify-ignore}

Devuelve todos los cupones disponibles en el sistema.

**Devuelve:**
```json
{
    "success": true,
    "cupones":[
        {
            "id": int,
            "descuento_porcentual": number,
            "descripcion": string
        },
        ...n cupones
    ]
}
```

### Conseguir cupones de un cliente
##### `GET` /usuarios/clientes/:id_cliente/cupones {docsify-ignore}

Devuelve todos los cupones que tiene un cliente para gastar. No se incluyen cupones gastados o vencidos.

**Devuelve:**
```json
{
    "success": true,
    "cupones":[
        {
            "id": int,
            "descuento_porcentual": number,
            "descripcion": string,
            "fecha_emision": date, //'YYYY-MM-DD'
            "fecha_vencimiento": date //'YYYY-MM-DD'
        },
        ...n cupones
    ]
}
```

### Asignar un cupón a un cliente
##### `POST` /usuarios/clientes/:id_cliente/cupones {docsify-ignore}

Le asigna un cupón a un cliente.

**Recibe:**
```json
{
    "id_cupon": int,
    "fecha_vencimiento": date //'YYYY-MM-DD'
}
```
**Devuelve:**
```json
{
    "success": true
}
```

## **Órdenes**

### Hacer una orden
##### `POST` /ordenes {docsify-ignore}

Recibe una orden de un cliente.

**Recibe:**
```json
{
    "id_cliente": int,
    "id_sucursal": int, //si es null se autoasigna basado en proximidad
    "productos":[
        {
            "id_producto": int,
            "cantidad": int                                                           
        },
        ...n productos
    ]
}
```
**Devuelve:**
```json
{
    "success": true,
    "id_orden": int,
    "id_sucursal": int
}
```

### Conseguir una orden
##### `GET` /sucursales/:id_sucursal/ordenes/:id_orden {docsify-ignore}

Devuelve la infomación de una orden.

**Devuelve:**
```json
{
    "success": true,
    "orden":{
        "id": int,
        "id_cliente": int,
        "id_sucursal": int,
        "emision": date, //'YYYY-MM-DD'
        "productos":[
            {
                "id": int,
                "cantidad": int,
                "precio": number
            },
            ...n productos
        ]
    }
}
```

### Evaluar una orden
##### `POST` /evaluaciones {docsify-ignore}

Recibe una evaluación para una orden de un cliente.

**Recibe:**
```json
{
    "id_orden": int,
    "id_sucursal": int,
    "evaluacion": int //del 1 al 5
}
```
**Devuelve:**
```json
{
    "success": true
}
```

### Asignar orden a mensajero para entregar
##### `POST` /sucursales/:id_sucursal/entregas {docsify-ignore}

Asigna una orden a un mensajero por entregar.

**Recibe:**
```json
{
    "id_mensajero": int,
    "id_orden": int
}
```
**Devuelve:**
```json
{
    "success": true
}
```

### Completar entrega de mensajero
##### `PUT` /sucursales/:id_sucursal/entregas {docsify-ignore}

Marca como entregada una orden de mensajero.

**Recibe:**
```json
{
    "id_mensajero": int,
    "id_orden": int
}
```
**Devuelve:**
```json
{
    "success": true
}
```

### Conseguir entregas pendientes
##### `GET` /sucursales/:id_sucursal/empleados/:id_mensajero/pendientes {docsify-ignore}

Devuelve todas las órdenes pendientes de un mensajero.

**Devuelve:**
```json
{
    "success": true,
    "ordenes":[
        {
            "id": int,
            "nombre_cliente": string,
            "emision": date, //'YYYY-MM-DD'
            "ubicacion":{
                "latitud": number,
                "longitud": number
            },
            "productos":[
                {
                    "id": int,
                    "cantidad": int,
                    "nombre": string,
                    "descripcion": string
                },
                ...n productos
            ]
        },
        ...n órdenes
    ]
}
```

### Conseguir entregas completadas
##### `GET` /sucursales/:id_sucursal/empleados/:id_mensajero/completadas {docsify-ignore}

Devuelve todas las órdenes completadas de un mensajero.

**Devuelve:**
```json
{
    "success": true,
    "ordenes":[
        {
            "id": int,
            "nombre_cliente": string,
            "ubicacion":{
                "latitud": number,
                "longitud": number
            },
            "productos":[
                {
                    "id": int,
                    "cantidad": int,
                    "nombre": string,
                    "descripcion": string
                },
                ...n productos
            ]
        },
        ...n órdenes
    ]
}
```

### Conseguir métodos de pago
##### `GET` /sucursales/:id_sucursal/metodos {docsify-ignore}

Devuelve los posibles métodos de pago en el sistema.

**Devuelve:**
```json
{
    "success": true,
    "metodos":[
        {
            "id": int,
            "nombre": int
        },
        ...n métodos
    ]
}
```

### Facturar una orden
##### `POST` /sucursales/:id_sucursal/facturas {docsify-ignore}

Recibe una factura para una orden de un cliente.

**Recibe:**
```json
{
    "id_orden": int,
    "id_metodo_pago": int,
    "id_vendedor": int,
    "id_cupon": int, //puede ser null si no se usa
    "id_oferta": int //puede ser null si no se usa
}
```
**Devuelve:**
```json
{
    "success": true,
    "id_factura": int
}
```

### Conseguir una factura
##### `GET` /sucursales/:id_sucursal/facturas/:id_factura {docsify-ignore}

Devuelve la infomación de una orden facturada.

**Devuelve:**
```json
{
    "success": true,
    "factura":{
        "id": int,
        "id_orden": int,
        "fecha_compra": date, //'YYYY-MM-DD'
        "subtotal": number,
        "total": number,
        "vendedor": {
            "id": int,
            "nombre": string
        },
        "cliente":{
            "id": int,
            "nombre": string
        },
        "metodo_pago": {
            "id": int,
            "nombre": string
        }
    }
}
```

---
## **Productos**

### Conseguir los productos disponibles
##### `GET` /productos {docsify-ignore}

Devuelve todos los productos disponibles en el taller. No toma en cuenta inventario de sucursales.

**Devuelve:**
```json
{
    "success": true,
    "productos":[
        {
            "id": int,
            "foto": string, //string base64 de la imagen
            "nombre": string,
            "precio_venta": number,
            "precio_costo": number,
            "descripcion": string
        }
    ]
}
```

### Conseguir un producto
##### `GET` /productos/:id_producto {docsify-ignore}

Devuelve un producto disponible en el taller.

**Devuelve:**
```json
{
    "success": true,
    "producto":{
        "id": int,
        "foto": string, //string base64 de la imagen
        "nombre": string,
        "precio_venta": number,
        "precio_costo": number,
        "descripcion": string
    }
}
```

### Agregar un producto
##### `POST` /productos {docsify-ignore}

Agrega un producto nuevo a la lista del taller.

**Recibe:**
```json
{
    "foto": string, //string base64 de la imagen
    "nombre": string,
    "precio_venta": number,
    "precio_costo": number,
    "descripcion": string
}
```
**Devuelve:**
```json
{
    "success": true
}
```

### Modificar un producto
##### `PUT` /productos/:id_producto {docsify-ignore}

Modifica la información de un producto en la lista del taller.

**Recibe:**
```json
{
    "foto": string, //string base64 de la imagen
    "nombre": string,
    "precio_venta": number,
    "precio_costo": number,
    "descripcion": string
}
```
**Devuelve:**
```json
{
    "success": true
}
```

### Conseguir inventario completo
##### `GET` /sucursales/:id_sucursal/inventario {docsify-ignore}

Devuelve el inventario existente en una sucursal.

**Devuelve:**
```json
{
    "success": true,
    "inventario": [
        {
            "id": int,
            "foto": string, //string base64 de la imagen
            "nombre": string,
            "precio_venta": number,
            "precio_costo": number,
            "descripcion": string,
            "cantidad": int,
            "sucursal":{
                "id": int,
                "nombre": 
            }
        },
        ...n productos
    ]
}
```

### Conseguir inventario de un producto
##### `GET` /sucursales/:id_sucursal/inventario/:id_producto {docsify-ignore}

Devuelve el inventario existente para un producto en una sucursal.

**Devuelve:**
```json
{
    "success": true,
    "producto":{
        "id": int,
        "foto": string, //string base64 de la imagen
        "nombre": string,
        "precio_venta": number,
        "precio_costo": number,
        "descripcion": string,
        "cantidad": int,
        "sucursal":{
            "id": int,
            "nombre": 
        }
    }
}
```


### Agregar inventario
##### `POST` /sucursales/:id_sucursal/inventario {docsify-ignore}

Agrega inventario de algún producto a una sucursal.

**Recibe:**
```json
{
    "id_producto": int,
    "cantidad": int
}
```
**Devuelve:**
```json
{
    "success": true
}
```

### Reducir inventario
##### `DELETE` /sucursales/:id_sucursal/inventario {docsify-ignore}

Reduce inventario de algún producto a una sucursal.

**Recibe:**
```json
{
    "id_producto": int,
    "cantidad": int
}
```
**Devuelve:**
```json
{
    "success": true
}
```

---
## **Empleados**

### Conseguir todos los empleados
##### `GET` /empleados {docsify-ignore}

Devuelve todos los empleados disponibles en el sistema.

**Devuelve:**
```json
{
    "success": true,
    "empleados": [
        {
            "id": int,
            "id_sucursal":int,
            "foto": string, //string base64 con la foto
            "nombre": string,
            "apellido": string,
            "salario": number,
            "telefono": string,
            "email": string,
            "cedula": string,
            "fecha_contratacion": date, //'YYYY-MM-DD'
            "puesto": {
                "id": int,
                "nombre":string
            }
        },
        ...n empleados
    ]
}
```

### Conseguir todos los vendedores
##### `GET` /vendedores {docsify-ignore}

Devuelve todos los empleados vendedores en el sistema.

**Devuelve:**
```json
{
    "success": true,
    "empleados": [
        {
            "id": int,
            "id_sucursal":int,
            "foto": string, //string base64 con la foto
            "nombre": string,
            "apellido": string,
            "salario": number,
            "telefono": string,
            "email": string,
            "cedula": string,
            "fecha_contratacion": date, //'YYYY-MM-DD'
            "puesto": {
                "id": int,
                "nombre":string
            }
        },
        ...n empleados
    ]
}
```

### Conseguir un empleado
##### `GET` /empleados/:id_empleado {docsify-ignore}

Devuelve un empleado disponible en recursos humanos.

**Devuelve:**
```json
{
    "success": true,
    "empleado":{
        "id": int,
        "id_sucursal":int,
        "foto": string, //string base64 con la foto
        "nombre": string,
        "apellido": string,
        "salario": number,
        "telefono": string,
        "email": string,
        "cedula": string,
        "fecha_contratacion": date, //'YYYY-MM-DD'
        "puesto": {
            "id": int,
            "nombre":string
        }
    }
}
```

### Agregar un empleado
##### `POST` /empleados {docsify-ignore}

Agrega un empleado nuevo en recursos humanos y en la sucural correspondiente en caso de ser especificada.

**Recibe:**
```json
{
    "id_sucursal":int,
    "id_puesto": int,
    "foto": string, //string base64 con la foto
    "nombre": string,
    "apellido": string,
    "telefono": string,
    "email": string,
    "password": string,
    "cedula": string
}
```
**Devuelve:**
```json
{
    "success": true
}
```

### Modificar un empleado
##### `PUT` /empleados/:id_empleado {docsify-ignore}

Modifica la información de un empleado en recursos humanos y y en la sucural correspondiente en caso de ser especificada.

**Recibe:**
```json
{
    "id_puesto": int,
    "foto": string, //string base64 con la foto
    "nombre": string,
    "apellido": string,
    "salario": number,
    "telefono": string,
    "email": string,
    "password": string,
    "cedula": string
}
```
**Devuelve:**
```json
{
    "success": true
}
```

---
## **Reportes**

### Generar reporte de ventas
##### `GET` /reportes/ventas? {docsify-ignore}

Genera un reporte de ventas. El query rerpresenta un filtro y puede recibir o omitir cualquiera de los siguientes valores:

* `id_sucursal`: Limita los resultados a una sucursal en particular.
* `id_producto`: Limita los valores a un producto en particular.
* `id_vendedor`: Limita los valores a un vendedor en particular.
* `fecha_inicio`: Indica la fecha inicial desde donde se debería calcular el reporte. Debe estar en formato `'YYYY-MM-DD'`.
* `fecha_final`: Indica la fecha final hasta donde se deberían calcular el reporte. Debe estar en formato `'YYYY-MM-DD'`.

**Devuelve:**
```json
{
    "success": true,
    "reporte":[
        {
            "sucursal": string,
            "producto": string,
            "vendedor": string,
            "ventas": number, //dinero
            "cantidad": int //unidades
        },
        ...n entradas
    ]
}
```

### Generar reporte de ganancias
##### `GET` /reportes/ganancias? {docsify-ignore}

Genera un reporte de ganancias. El query rerpresenta un filtro y puede recibir o omitir cualquiera de los siguientes valores:

* `id_sucursal`: Limita los resultados a una sucursal en particular.
* `id_producto`: Limita los valores a un producto en particular.
* `id_vendedor`: Limita los valores a un vendedor en particular.
* `fecha_inicio`: Indica la fecha inicial desde donde se debería calcular el reporte. Debe estar en formato `'YYYY-MM-DD'`.
* `fecha_final`: Indica la fecha final hasta donde se deberían calcular el reporte. Debe estar en formato `'YYYY-MM-DD'`.

**Devuelve:**
```json
{
    "success": true,
    "reporte": [
        {
            "sucursal": string,
            "producto": string,
            "vendedor": string,
            "ganancias": number
        },
        ...n entradas
    ]
}
```
# 