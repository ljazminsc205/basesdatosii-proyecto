USE Taller
GO

CREATE OR ALTER PROC agregarProducto @imgProd varchar(max), @nombre varchar(25), @precioVenta decimal (20,2), @precioCosto decimal (20,2), @descripcion varchar(100) AS
BEGIN
	INSERT INTO FotoProducto VALUES 
		(@imgProd)
	INSERT INTO Producto VALUES
		(IDENT_CURRENT('FotoProducto'), @nombre, @precioVenta, @precioCosto, @descripcion)
END;
GO

CREATE OR ALTER PROC verProductos AS
BEGIN
	SELECT Producto.id AS id, nombre AS nombre, descripcion AS descripcion, precio_venta AS precio_venta, precio_costo AS precio_costo, FotoProducto.imagen AS foto
		FROM Producto JOIN FotoProducto ON Producto.id_foto = FotoProducto.id;
END;
GO

CREATE OR ALTER PROC verProd @idProd int AS
BEGIN
	SELECT Producto.id AS id, FotoProducto.imagen AS foto, Producto.nombre AS nombre, Producto.precio_venta AS precio_venta, Producto.precio_costo AS precio_costo, Producto.descripcion AS descripcion
		FROM Producto JOIN FotoProducto ON Producto.id_foto = FotoProducto.id WHERE Producto.id = @idProd
END;
GO

CREATE OR ALTER PROC verEmpleados AS
BEGIN
	SELECT Empleado.id AS id, Empleado.nombre AS nombre, Empleado.apellido AS apellidos, RecursosHumanos..FotoEmpleado.imagen AS foto, Empleado.cedula AS cedula,
		RecursosHumanos..Puesto.nombre AS puesto, Empleado.salario AS salario, Empleado.telefono AS telefono, Empleado.email AS correo, Empleado.fecha_contratacion AS fecha_contrato
			FROM Empleado JOIN RecursosHumanos..FotoEmpleado ON Empleado.id_foto = FotoEmpleado.id JOIN RecursosHumanos..Puesto ON Empleado.id_puesto = Puesto.id
END;
GO

CREATE OR ALTER PROC buscarEmpleado @idEmpleado int AS
BEGIN
	SELECT Empleado.id AS id, Empleado.nombre AS nombre, Empleado.apellido AS apellidos, RecursosHumanos..FotoEmpleado.imagen AS foto, Empleado.cedula AS cedula,
		RecursosHumanos..Puesto.nombre AS puesto, Empleado.salario AS salario, Empleado.telefono AS telefono, Empleado.email AS correo, Empleado.fecha_contratacion AS fecha_contrato
			FROM Empleado JOIN RecursosHumanos..FotoEmpleado ON Empleado.id_foto = FotoEmpleado.id JOIN RecursosHumanos..Puesto ON Empleado.id_puesto = Puesto.id
		WHERE Empleado.id = @idEmpleado
END;
GO

CREATE OR ALTER PROC updateProd @id int, @foto varchar(max) = NULL, @nombre varchar(25) = NULL, 
			@pventa decimal (20,2) = NULL, @pcosto decimal (20,2) = NULL, @desc varchar(100) = NULL AS
BEGIN
	UPDATE Taller..FotoProducto SET Taller..FotoProducto.imagen = ISNULL(@foto, Taller..FotoProducto.imagen) 
		WHERE (SELECT Taller..Producto.id_foto FROM Taller..Producto WHERE Taller..Producto.id = @id) = Taller..FotoProducto.id
	UPDATE Taller..Producto SET Taller..Producto.nombre = ISNULL(@nombre, Taller..Producto.nombre),
		Taller..Producto.precio_venta = ISNULL(@pventa, Taller..Producto.precio_venta),
		Taller..Producto.precio_costo= ISNULL(@pcosto, Taller..Producto.precio_costo), 
		Taller..Producto.descripcion = ISNULL(@desc, Taller..Producto.descripcion) WHERE Taller..Producto.id = @id
END;
GO