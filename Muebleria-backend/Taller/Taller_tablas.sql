CREATE DATABASE [Taller];
GO

USE [Taller]

CREATE TABLE [Empleado] (
  [id] int NOT NULL,
  [id_foto] int UNIQUE NOT NULL,
  [id_puesto] int UNIQUE NOT NULL,
  [nombre] varchar(25) NOT NULL,
  [apellido] varchar(50) NOT NULL,
  [salario] decimal (20,2) NOT NULL,
  [telefono] varchar(8) NOT NULL,
  [email] varchar(25) UNIQUE NOT NULL,
  [contrasena] varchar(25) NOT NULL,
  [fecha_contratacion] datetime NOT NULL,
  [cedula] varchar(12) UNIQUE NOT NULL,

  PRIMARY KEY ([id])
);

--CREATE INDEX [FK] ON  [Empleado] ([id_foto], [id_puesto]);

CREATE TABLE [FotoProducto] (
  [id] int IDENTITY(0, 1) NOT NULL,
  [imagen] varchar(max) NOT NULL,

  PRIMARY KEY ([id])
);

CREATE TABLE [Producto] (
  [id] int IDENTITY(0, 1) NOT NULL,
  [id_foto] int NOT NULL,
  [nombre] varchar(25) NOT NULL,
  [precio_venta] decimal (20,2) NOT NULL,
  [precio_costo] decimal (20,2) NOT NULL,
  [descripcion] varchar(100) NOT NULL,
  
  PRIMARY KEY ([id]),
  FOREIGN KEY ([id_foto]) REFERENCES [FotoProducto]([id])
);
