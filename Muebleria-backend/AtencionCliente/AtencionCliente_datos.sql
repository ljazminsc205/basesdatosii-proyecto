USE `AtencionCliente`;

INSERT INTO `Empleado` ( `id`, `id_foto`, `id_puesto`, `nombre`, `apellido`, `salario`, `telefono`, `email`, `contrasena`, `fecha_contratacion`, `cedula` ) VALUES 
    (2, 2, 2, 'TOMAS', 'POSTIGO VELAZQUEZ', 25000, '86206916', 'topovel@correo.com', 'topo41', '2020-06-30', '809879811'),
    (3, 3, 2, 'EVA MARIA', 'MAURI AGUSTIN', 25000, '48445860', 'evmauag@correo.com', 'evama72', '2020-02-23', '828416193');

INSERT INTO `Cliente` (`ubicacion`, `nombre`, `apellido`, `fecha_nacimiento`, `email`, `password`) VALUES
    (POINT(10.05734, -85.41811), 'Alejandro', 'Alvarado', '1994-08-10', 'aalvarado@correo.com', 'equisde'),
    (POINT(9.9255, -84.0791), 'Bernardo', 'Badilla', '1990-05-02', 'bbadilla@correo.com', 'equisde'),
    (POINT(9.94266, -84.08665), 'Carlos', 'Contreras', '1984-08-10', 'ccontreras@correo.com', 'equisde'),
    (POINT(9.9815, -84.1378), 'Diana', 'Derulo', '1940-03-13', 'dderulo@correo.com', 'equisde'),
    (POINT(9.84516, -83.94895), 'Elena', 'Alvarado', '2004-10-16', 'ealvarado@correo.com', 'equisde');

INSERT INTO `TipoTelefono` (`nombre`) VALUES
	('Celular'),
    ('Oficina'),
    ('Hogar');

INSERT INTO `Telefono` (`id_cliente`, `id_tipo`, `numero`) VALUES
	(1, 1, 88882304),
    (1, 2, 25533406),
    (2, 1, 87230912),
    (3, 1, 85412304),
    (4, 1, 88602019),
    (5, 1, 88903407),
    (5, 3, 22304591);

INSERT INTO `Oferta` (`descuento_porcentual`, `descripcion`, `fecha_emision`, `fecha_vencimiento`) VALUES
	(10, 'Oferta prueba', '2020-08-01', '2020-08-31');

INSERT INTO `Cupon` (`descuento_porcentual`, `descripcion`) VALUES
	(10, 'Cumpleaños'),
    (15, 'Regalía por encuesta');