USE `AtencionCliente`;

DELIMITER //

DROP PROCEDURE IF EXISTS `sp_AgregarCliente`//
CREATE PROCEDURE `sp_AgregarCliente`(
	_latitud float,
    _longitud float,
	_nombre varchar(25),
    _apellido varchar(50),
    _fecha_nacimiento date,
    _email varchar(25),
    _password varchar(25)
)
BEGIN
	/*revisar validez del email*/
	IF NOT _email REGEXP  '^[a-z0-9\._%+!$&*=^|~#%\'`?{}/\-]+@[a-z0-9\.-]+\.[a-z]{2,6}$' THEN
		SIGNAL SQLSTATE '45000'
			SET MESSAGE_TEXT = 'Email inválido.';
	END IF;
	INSERT INTO `Cliente` (`ubicacion`, `nombre`, `apellido`, `fecha_nacimiento`, `email`, `password`) VALUES
		(POINT(_latitud, _longitud), _nombre, _apellido, _fecha_nacimiento, _email, _password);
END //

DROP PROCEDURE IF EXISTS `sp_ModificarCliente`//
CREATE PROCEDURE `sp_ModificarCliente`(
	_id int,
	_latitud float,
    _longitud float,
	_nombre varchar(25),
    _apellido varchar(50),
    _fecha_nacimiento date,
    _email varchar(25),
    _password varchar(25)
)
BEGIN
	/*revisar validez del email*/
	IF NOT _email REGEXP  '^[a-z0-9\._%+!$&*=^|~#%\'`?{}/\-]+@[a-z0-9\.-]+\.[a-z]{2,6}$' THEN
		SIGNAL SQLSTATE '45000'
			SET MESSAGE_TEXT = 'Email inválido.';
	END IF;
    
    /*latitud y longitud viejos en caso de nulls como parámetros*/
    SELECT @latitud := ST_X(`ubicacion`), @longitud := ST_Y(`ubicacion`) 
    FROM `Cliente`
    WHERE `Cliente`.`id` = _id;
    
	UPDATE `Cliente`
    SET
		`ubicacion` = POINT(ifnull(_latitud, @latitud), ifnull(_longitud, @longitud)),
        `nombre` = ifnull(_nombre, `Cliente`.`nombre`),
        `apellido` = ifnull(_apellido, `Cliente`.`apellido`),
        `fecha_nacimiento` = ifnull(_fecha_nacimiento, `Cliente`.`fecha_nacimiento`),
        `email` = ifnull(_email, `Cliente`.`email`),
        `password` = ifnull(_password, `Cliente`.`password`)
	WHERE `Cliente`.`id` = _id;
END //

DROP PROCEDURE IF EXISTS `sp_BuscarCliente`//
CREATE PROCEDURE `sp_BuscarCliente`(
	_id int
)
BEGIN
	SELECT `id`, `ubicacion`, `nombre`, `apellido`, `fecha_nacimiento`, `email`
    FROM `Cliente`
    WHERE `Cliente`.`id` = ifnull(_id, `Cliente`.`id`);
END //

DROP PROCEDURE IF EXISTS `sp_BuscarOferta`//
CREATE PROCEDURE `sp_BuscarOferta`(
	_id int
)
BEGIN
	SELECT `id`, `descuento_porcentual`, `descripcion`, `fecha_emision`, `fecha_vencimiento`
    FROM `Oferta`
    WHERE `Oferta`.`id` = ifnull(_id, `Oferta`.`id`);
END //

DROP PROCEDURE IF EXISTS `sp_BuscarCupon`//
CREATE PROCEDURE `sp_BuscarCupon`(
	_id int
)
BEGIN
	SELECT `id`, `descuento_porcentual`, `descripcion`
    FROM `Cupon`
    WHERE `Cupon`.`id` = ifnull(_id, `Cupon`.`id`);
END //

DROP PROCEDURE IF EXISTS `sp_BuscarCuponPorCliente`//
CREATE PROCEDURE `sp_BuscarCuponPorCliente`(
    _id_cliente int
)
BEGIN
	SELECT `Cupon`.`id` as id, `Cupon`.`descripcion` as descripcion, `Cupon`.`descuento_porcentual` as descuento_porcentual, `Cupon`.`descripcion` as descripcion, `CuponxCliente`.`fecha_emision` as fecha_emision, `CuponxCliente`.`fecha_vencimiento` as fecha_vencimiento
    FROM `Cupon` INNER JOIN `CuponxCliente`
    ON `Cupon`.`id` = `CuponxCliente`.`id_cupon`
    WHERE `CuponxCliente`.`id_cliente` = _id_cliente AND
		CURDATE() < `CuponxCliente`.`fecha_vencimiento` AND
        gastado = false;
END //

DROP PROCEDURE IF EXISTS `sp_AsignarCuponACliente`//
CREATE PROCEDURE `sp_AsignarCuponACliente`(
	_id_cupon int,
    _id_cliente int,
    _fecha_vencimiento date
)
BEGIN
	INSERT INTO `CuponxCliente`(`id_cupon`, `id_cliente`, `fecha_emision`, `fecha_vencimiento`, `gastado`) VALUES
		(_id_cupon, _id_cliente, CURDATE(), _fecha_vencimiento, false);
END //

DROP PROCEDURE IF EXISTS `sp_AgregarTelefono`//
CREATE PROCEDURE `sp_AgregarTelefono`(
	_id_cliente int,
    _id_tipoTelefono int,
    _numero varchar(8)
)
BEGIN
	/*revisar que sea un número válido*/
	IF _numero REGEXP  '[^0-9]+' THEN
		SIGNAL SQLSTATE '45000'
			SET MESSAGE_TEXT = 'Número de teléfono inválido. Incluya únicamente 8 dígitos numéricos.';
	END IF;
	INSERT INTO `Telefono`(`id_cliente`, `id_tipo`, `numero`) VALUES
		(_id_cliente, _id_tipoTelefono, _numero);
END //

DROP PROCEDURE IF EXISTS `sp_ModificarTelefono`//
CREATE PROCEDURE `sp_ModificarTelefono`(
	_id int,
    _id_cliente int,
	_id_tipoTelefono int,
    _numero varchar(8)
)
BEGIN
	/*revisar que sea un número válido*/
	IF _numero REGEXP  '[^0-9]+' THEN
		SIGNAL SQLSTATE '45000'
			SET MESSAGE_TEXT = 'Número de teléfono inválido. Incluya únicamente 8 dígitos numéricos.';
	END IF;
    
	UPDATE `Telefono`
    SET
		`id_tipo` = ifnull(_id_tipoTelefono, `Telefono`.`id_tipo`),
        `numero` = ifnull(_numero, `Telefono`.`numero`)
	WHERE `Telefono`.`id` = id AND `Telefono`.`id_cliente` = _id_cliente;
END //

DROP PROCEDURE IF EXISTS `sp_BuscarTelefonoPorCliente`//
CREATE PROCEDURE `sp_BuscarTelefonoPorCliente`(
    _id_cliente int
)
BEGIN
	SELECT `id`, `id_cliente`, `id_tipo`, `numero`
    FROM `Telefono`
    WHERE `Telefono`.`id_cliente` = _id_cliente;
END //

DROP PROCEDURE IF EXISTS `sp_BuscarTipoTelefono`//
CREATE PROCEDURE `sp_BuscarTipoTelefono`()
BEGIN
	SELECT `id`, `nombre`
    FROM `TipoTelefono`;
END //

DROP PROCEDURE IF EXISTS `sp_AgregarEmpleado`//
CREATE PROCEDURE `sp_AgregarEmpleado`(
	_id_foto int,
    _id_puesto int,
	_nombre varchar(25),
    _apellido varchar(50),
    _salario decimal,
    _telefono varchar(8),
    _email varchar(25),
    _contrasena varchar(25),
    _fecha_contratacion date,
    _cedula varchar(12)
)
BEGIN
	/*revisar validez del email*/
	IF NOT _email REGEXP  '^[a-z0-9\._%+!$&*=^|~#%\'`?{}/\-]+@[a-z0-9\.-]+\.[a-z]{2,6}$' THEN
		SIGNAL SQLSTATE '45000'
			SET MESSAGE_TEXT = 'Email inválido.';
	END IF;
    
    /*revisar validez del telefono*/
    IF _telefono REGEXP '[^0-9]+' THEN
		SIGNAL SQLSTATE '45000'
			SET MESSAGE_TEXT = 'Número de teléfono inválido. Incluya únicamente 8 dígitos numéricos.';
	END IF;
    
    /*revisar validez de la cedula*/
    IF _cedula REGEXP '[^0-9]+' THEN
		SIGNAL SQLSTATE '45000'
			SET MESSAGE_TEXT = 'Número de cédula inválido. Incluya únicamente los dígitos numéricos.';
	END IF;
    
	INSERT INTO `Empleado` (`id_foto`, `id_puesto`, `nombre`, `apellido`, `salario`, `telefono`, `email`, `contrasena`, `fecha_contratacion`, `cedula`) VALUES
		(_id_foto, _id_puesto, _nombre, _apellido, _salario, _telefono, _email, _contrasena, _fecha_contratacion, _cedula);
END //

DELIMITER ;