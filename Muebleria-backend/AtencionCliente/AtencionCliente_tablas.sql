DROP DATABASE IF EXISTS `AtencionCliente`;
CREATE DATABASE `AtencionCliente`;
USE `AtencionCliente`;

CREATE TABLE `Empleado` (
  `id` int NOT NULL,
  `id_foto` int NOT NULL,
  `id_puesto` int NOT NULL,
  `nombre` varchar(25) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `salario` decimal NOT NULL,
  `telefono` varchar(8) NOT NULL,
  `email` varchar(25) UNIQUE NOT NULL,
  `contrasena` varchar(25) NOT NULL,
  `fecha_contratacion` date NOT NULL,
  `cedula` varchar(12) UNIQUE NOT NULL,

  PRIMARY KEY (`id`)
);

/*CREATE INDEX [FK] ON  [Empleado] ([id_foto], [id_puesto]);*/

CREATE TABLE `Cliente` (
  `id` int AUTO_INCREMENT NOT NULL,
  `ubicacion` geometry NOT NULL,
  `nombre` varchar(25) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `email` varchar(25) UNIQUE NOT NULL,
  `password` varchar(25) NOT NULL,

  PRIMARY KEY (`id`)
);

CREATE TABLE `TipoTelefono` (
  `id` int AUTO_INCREMENT NOT NULL,
  `nombre` varchar(25) NOT NULL,

  PRIMARY KEY (`id`)
);

CREATE TABLE `Telefono` (
  `id` int AUTO_INCREMENT NOT NULL,
  `id_cliente` int NOT NULL,
  `id_tipo` int NOT NULL,
  `numero` varchar(8) NOT NULL,

  PRIMARY KEY (`id`),
  FOREIGN KEY (`id_cliente`) REFERENCES `Cliente`(`id`),
  FOREIGN KEY (`id_tipo`) REFERENCES `TipoTelefono`(`id`)
);

CREATE TABLE `Oferta` (
  `id` int AUTO_INCREMENT NOT NULL,
  `descuento_porcentual` float NOT NULL,
  `descripcion` varchar(25) NOT NULL,
  `fecha_emision` date NOT NULL,
  `fecha_vencimiento` date NOT NULL,

  PRIMARY KEY (`id`)
);

CREATE TABLE `Cupon` (
  `id` int AUTO_INCREMENT NOT NULL,
  `descuento_porcentual` float NOT NULL,
  `descripcion` varchar(25) NOT NULL,

  PRIMARY KEY (`id`)
);

CREATE TABLE `CuponxCliente` (
  `id_cupon` int NOT NULL,
  `id_cliente` int NOT NULL,
  `fecha_emision` date NOT NULL,
  `fecha_vencimiento` date NOT NULL,
  `gastado` boolean,

  PRIMARY KEY (`id_cupon`, `id_cliente`),
  FOREIGN KEY (`id_cupon`) REFERENCES `Cupon`(`id`),
  FOREIGN KEY (`id_cliente`) REFERENCES `Cliente`(`id`)
);