-- SE RECOMIENDA CORRE EL SCRIPT DE PROCS PRIMERO

EXEC sp_MSforeachdb 'IF ''?'' IN (''Sucursal1'', ''Sucursal2'', ''Sucursal3'', ''Sucursal4'') BEGIN USE ?
								INSERT INTO MetodoPago VALUES
									(''Efectivo''),
									(''Tarjeta de Crédito'')
					END;'
GO

USE Sucursal1
GO

INSERT INTO Empleado VALUES
	(4, 4, 3, 'MARTA', 'NICOLAS BARRIONUEVO', 25000, '79291134', 'maniba@correo.com', 'bruceW', '2020-03-17', '425932091'),
	(5, 5, 4, 'ALFONSO', 'GRAU ARENCIBIA', 25000, '22077930', 'algrar@correo.com', 'elrick', '2020-04-24', '553618120'),
	(6, 6, 5, 'JOSE', 'POSE SANTANDER', 25000, '97851112', 'joposa@correo.com', 'josepo', '2020-03-27', '042215385'),
	(7, 7, 6, 'OLGA', 'CURIEL VIANA', 30000, '44309091', 'olcuvi@correo.com', 'olgacuvi', '2020-07-17', '171651131')
GO

INSERT INTO Inventario VALUES
	(3, 0, 4),
	(2, 0, 1),
	(9, 0, 2),
	(5, 0, 1)
GO

EXEC crearOrden 1, 0

EXEC agregarDetalle 0, 3, 1
EXEC agregarDetalle 0, 3, 1
EXEC agregarDetalle 0, 9, 1

EXEC facturar 0, 6, 0

USE Sucursal2
GO

INSERT INTO Empleado VALUES
	(8, 8, 3, 'FELIPE', 'LORCA SAN MARTIN', 25000, '59543569', 'felosama@correo.com', 'santomartin', '2020-05-05', '999358841'),
	(9, 9, 4, 'LORENA', 'ROPERO JORGE', 25000, '65380296', 'lorojo@correo.com', 'lorepo', '2020-03-12', '412621896'),
	(10, 10, 5, 'JUAN FRANCISCO', 'VILLAREJO GUISADO', 25000, '98886802', 'jufavigu@correo.com', 'guisado25', '2020-04-16', '545367043'),
	(11, 11, 6, 'ROSA', 'MIQUEL OLIVARES', 30000, '46030587', 'romiol@correo.com', 'rosital', '2020-05-25', '729830334')
GO

INSERT INTO Inventario VALUES
	(1, 1, 4),
	(0, 1, 2),
	(7, 1, 2),
	(5, 1, 3)
GO

EXEC crearOrden 2, 1
EXEC crearOrden 1, 1

EXEC agregarDetalle 0, 5, 2
EXEC agregarDetalle 1, 7, 1

EXEC facturar 1, 10, 0
EXEC facturar 0, 10, 1

EXEC agregarPedidoMensajero 0, 9
EXEC agregarPedidoMensajero 1, 9

EXEC actualizarPedido 0, 1

EXEC agregarEvaluacion 0, 5

USE Sucursal3
GO

INSERT INTO Empleado VALUES
	(12, 12, 3, 'VICTOR MARTIN', 'PEÑA MILLAN', 25000, '81767635', 'vimapemi@correo.com', 'vickymartin', '2020-03-30', '551753674'),
	(13, 13, 4, 'ALBA', 'LLADO GEORGIEVA', 25000, '90084023', 'alllge@correo.com', 'tripleL', '2020-04-09', '067350387'),
	(14, 14, 5, 'JORGE', 'JORDAN TORREGROSA', 25000, '51935217', 'jojoto@correo.com', 'mojojojo', '2020-05-19', '610447571'),
	(15, 15, 6, 'ISABEL', 'MIRANDA GRANDA', 30000, '58793124', 'ismigra@correo.com', 'chabela77', '2020-02-29', '115597166')
GO

INSERT INTO Inventario VALUES
	(6, 2, 2),
	(8, 2, 2),
	(3, 2, 2),
	(1, 2, 2)
GO

EXEC crearOrden 3, 2

EXEC agregarDetalle 0, 8, 1

EXEC facturar 1, 14, 0

USE Sucursal4
GO

INSERT INTO Empleado VALUES
	(16, 16, 3, 'HUGO', 'BLAS CORTINA', 25000, '74319806', 'hublor@correo.com', 'blascort', '2020-04-07', '974626983'),
	(17, 17, 4, 'LAURA', 'ESCRIVA MUNUERA', 25000, '81308637', 'lauesmu@correo.com', 'laura83', '2020-01-06', '867752614'),
	(18, 18, 5, 'JOSE FRANCISCO', 'GARCIA ALMEIDA', 25000, '85035960', 'jofagal@correo.com', 'almendra52', '2020-06-05', '777381979'),
	(19, 19, 6, 'VERONICA', 'LARIOS ZAMBRANA', 30000, '97347076', 'velaza@correo.com', 'verolaza', '2020-02-04', '447454976')
GO

INSERT INTO Inventario VALUES
	(8, 3, 2),
	(9, 3, 5),
	(2, 3, 2),
	(3, 3, 3)
GO

EXEC crearOrden 2, 3

EXEC agregarDetalle 0, 9, 3

EXEC facturar 0, 18, 0