create database Sucursal1;
create database Sucursal2;
create database Sucursal3;
create database Sucursal4;
GO

EXEC sp_MSforeachdb 'IF ''?'' IN (''Sucursal1'', ''Sucursal2'', ''Sucursal3'', ''Sucursal4'') BEGIN USE ?
				CREATE TABLE [MetodoPago] (
					[id] int IDENTITY(0, 1) PRIMARY KEY,
					[nombre] varchar(25)
				);

				CREATE TABLE [Empleado] (
				  [id] int PRIMARY KEY,
				  [id_foto] int NOT NULL,
				  [id_puesto] int NOT NULL,
				  [nombre] varchar(25) NOT NULL,
				  [apellido] varchar(50) NOT NULL,
				  [salario] decimal (20,2) NOT NULL,
				  [telefono] varchar(8) NOT NULL,
				  [email] varchar(25) NOT NULL,
				  [contrasena] varchar(25) NOT NULL,
				  [fecha_contratacion] datetime NOT NULL,
				  [cedula] varchar(12) NOT NULL
				);
	
				CREATE TABLE [Orden] (
					[id] int IDENTITY(0, 1) PRIMARY KEY,
					[id_cliente] int NOT NULL,
					[id_sucursal] int NOT NULL,
					[emision] datetime NOT NULL
				);

				CREATE TABLE [DetalleOrden] (
				  [id_orden] int FOREIGN KEY REFERENCES Orden(id) NOT NULL,
				  [id_producto] int NOT NULL,
				  [cantidad] int NOT NULL,
				  [precio] decimal (20,2) NOT NULL,
				  PRIMARY KEY( id_orden, id_producto)
				);

				CREATE TABLE [Factura] (
				  [id] int IDENTITY(0, 1) PRIMARY KEY NOT NULL,
				  [id_metodoPago] int FOREIGN KEY REFERENCES MetodoPago(id) NOT NULL,
				  [id_vendedor] int FOREIGN KEY REFERENCES Empleado(id),
				  [id_orden] int FOREIGN KEY REFERENCES Orden(id) UNIQUE NOT NULL,
				  [fecha_compra] datetime NOT NULL
				);
			END;'
GO

EXEC sp_MSforeachdb 'IF ''?'' IN (''Sucursal1'', ''Sucursal2'', ''Sucursal3'', ''Sucursal4'') BEGIN USE ?
				CREATE TABLE [EvaluacionEnvio] (
				  [id] int IDENTITY(0, 1) PRIMARY KEY NOT NULL,
				  [id_orden] int FOREIGN KEY REFERENCES Orden(id) NOT NULL,
				  [evaluacion] int NOT NULL,
				  [fecha] datetime NOT NULL
				);
		
				CREATE TABLE [CuponxFactura] (
					[id_factura] int FOREIGN KEY REFERENCES Factura(id) NOT NULL,
					[id_cupon] int NOT NULL,
					PRIMARY KEY(id_factura, id_cupon)
				);

				CREATE TABLE [OfertaxFactura] (
					[id_factura] int FOREIGN KEY REFERENCES Factura(id),
					[id_oferta] int NOT NULL,
					PRIMARY KEY (id_factura, id_oferta)
				);

				CREATE TABLE [Inventario] (
					[id_producto] int NOT NULL,
					[id_sucursal] int NOT NULL,
					[cantidad] int NOT NULL,
					PRIMARY KEY(id_producto, id_sucursal)
				);

				CREATE TABLE [OrdenxMensajero] (
					[id_orden] int FOREIGN KEY REFERENCES Orden(id) NOT NULL,
					[id_mensajero] int FOREIGN KEY REFERENCES Empleado(id) NOT NULL,
					[entregado] bit NOT NULL,
					PRIMARY KEY(id_orden, id_mensajero)
				);
			END'
GO