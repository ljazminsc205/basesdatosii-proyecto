DECLARE @PROCS VARCHAR(MAX)
SELECT @PROCS = 'IF ''?'' IN (''Sucursal1'', ''Sucursal2'', ''Sucursal3'', ''Sucursal4'') BEGIN USE ? EXEC(''
	CREATE OR ALTER PROC verInventario AS
	BEGIN
		SELECT Taller..Producto.id AS id, Taller..Producto.nombre AS nombre, Taller..Producto.descripcion AS descripcion, Taller..Producto.precio_venta AS precio_venta,
			Taller..Producto.precio_costo AS precio_costo, Inventario.cantidad AS cantidad, Inventario.id_sucursal AS sucursal, RecursosHumanos..Sucursal.nombre AS nombre_sucursal,
			 Taller..FotoProducto.imagen AS foto
			FROM Inventario	JOIN Taller..Producto ON Inventario.id_producto = Taller..Producto.id
				JOIN Taller..FotoProducto ON Taller..Producto.id_foto = Taller..FotoProducto.id
					JOIN RecursosHumanos..Sucursal ON RecursosHumanos..Sucursal.id = Inventario.id_sucursal
	END;'')
	END'
EXEC sp_MSforeachdb @PROCS;
GO

DECLARE @PROCS VARCHAR(MAX)
SELECT @PROCS = 'IF ''?'' IN (''Sucursal1'', ''Sucursal2'', ''Sucursal3'', ''Sucursal4'') BEGIN USE ? EXEC(''
	CREATE OR ALTER PROC buscarProd @id int AS
	BEGIN
		SELECT Taller..Producto.id AS id, Taller..Producto.nombre AS nombre, Taller..Producto.descripcion AS descripcion, Taller..Producto.precio_venta AS precio_venta,
			Taller..Producto.precio_costo AS precio_costo, Inventario.cantidad AS cantidad, Inventario.id_sucursal AS sucursal, RecursosHumanos..Sucursal.nombre AS nombre_sucursal,
			 Taller..FotoProducto.imagen AS foto
			FROM Inventario	JOIN Taller..Producto ON Inventario.id_producto = Taller..Producto.id
				JOIN Taller..FotoProducto ON Taller..Producto.id_foto = Taller..FotoProducto.id
					JOIN RecursosHumanos..Sucursal ON RecursosHumanos..Sucursal.id = Inventario.id_sucursal
			WHERE Inventario.id_producto = @id
	END;'')
	END'
EXEC sp_MSforeachdb @PROCS;
GO

DECLARE @PROCS VARCHAR(MAX)
SELECT @PROCS = 'IF ''?'' IN (''Sucursal1'', ''Sucursal2'', ''Sucursal3'', ''Sucursal4'') BEGIN USE ? EXEC(''
	CREATE OR ALTER PROC reducirProd @id int, @cantidad int AS
	BEGIN
		UPDATE Inventario SET cantidad = cantidad - @cantidad WHERE id_producto = @id
		IF (SELECT cantidad FROM Inventario WHERE id_producto = @id) <= 0
			DELETE FROM Inventario WHERE Inventario.id_producto = @id
	END;'')
	END'
EXEC sp_MSforeachdb @PROCS;
GO

DECLARE @PROCS VARCHAR(MAX)
SELECT @PROCS = 'IF ''?'' IN (''Sucursal1'', ''Sucursal2'', ''Sucursal3'', ''Sucursal4'') BEGIN USE ? EXEC(''
	CREATE OR ALTER PROC agregarProd @idProd int, @idSucursal int, @cantidad int AS
	BEGIN
		IF NOT EXISTS (SELECT 1 FROM Inventario WHERE id_producto = @idProd)
			BEGIN
				INSERT INTO Inventario VALUES (@idProd, @idSucursal, @cantidad)
			END
		ELSE
			BEGIN
				UPDATE Inventario SET cantidad = cantidad + @cantidad WHERE id_producto = @idProd
			END
	END;'')
	END'
EXEC sp_MSforeachdb @PROCS;
GO

DECLARE @PROCS VARCHAR(MAX)
SELECT @PROCS = 'IF ''?'' IN (''Sucursal1'', ''Sucursal2'', ''Sucursal3'', ''Sucursal4'') BEGIN USE ? EXEC(''
	CREATE OR ALTER PROC infoGerente @idGerente int AS
	BEGIN
		SELECT nombre AS nombre, apellido AS apellidos, cedula AS cedula, salario AS salario, telefono AS telefono, email AS correo
			FROM Empleado WHERE id = @idGerente
	END;'')
	END'
EXEC sp_MSforeachdb @PROCS;
GO

DECLARE @PROCS VARCHAR(MAX)
SELECT @PROCS = 'IF ''?'' IN (''Sucursal1'', ''Sucursal2'', ''Sucursal3'', ''Sucursal4'') BEGIN USE ? EXEC(''
	CREATE OR ALTER PROC verPedidosEntregados @idMensajero int AS
	BEGIN
		SELECT OrdenxMensajero.id_orden AS id, nombre + '''' '''' + apellido AS nombre_cliente, Cliente.latitud AS latitud, Cliente.longitud AS longitud
			FROM OrdenxMensajero JOIN Orden ON OrdenxMensajero.id_orden = Orden.id
				JOIN openquery(ATENCIONCLIENTE,''''select id, nombre, apellido, ST_X(ubicacion) AS latitud, ST_Y(ubicacion) AS longitud from AtencionCliente.cliente'''') 
					AS cliente ON cliente.id = Orden.id_cliente
			WHERE id_mensajero = @idMensajero AND entregado = 1
	END;'')
	END'
EXEC sp_MSforeachdb @PROCS;
GO

DECLARE @PROCS VARCHAR(MAX)
SELECT @PROCS = 'IF ''?'' IN (''Sucursal1'', ''Sucursal2'', ''Sucursal3'', ''Sucursal4'') BEGIN USE ? EXEC(''
	CREATE OR ALTER PROC verPedidosPendientes @idMensajero int AS
	BEGIN
		SELECT OrdenxMensajero.id_orden AS id, nombre + '''' '''' + apellido AS nombre_cliente, Cliente.latitud AS latitud, Cliente.longitud AS longitud
			FROM OrdenxMensajero JOIN Orden ON OrdenxMensajero.id_orden = Orden.id
				JOIN openquery(ATENCIONCLIENTE,''''select id, nombre, apellido, ST_X(ubicacion) AS latitud, ST_Y(ubicacion) AS longitud from AtencionCliente.cliente'''') 
					AS cliente ON cliente.id = Orden.id_cliente
			WHERE id_mensajero = @idMensajero AND entregado = 0
	END;'')
	END'
EXEC sp_MSforeachdb @PROCS;
GO

DECLARE @PROCS VARCHAR(MAX)
SELECT @PROCS = 'IF ''?'' IN (''Sucursal1'', ''Sucursal2'', ''Sucursal3'', ''Sucursal4'') BEGIN USE ? EXEC(''
	CREATE OR ALTER PROC verPedido @idPedido int AS
	BEGIN
		SELECT Taller..Producto.id AS id, Taller..Producto.nombre AS nombre, Taller..Producto.descripcion AS descripcion, DetalleOrden.cantidad AS cantidad 
			FROM Orden JOIN DetalleOrden ON Orden.id = DetalleOrden.id_orden JOIN Taller..Producto ON DetalleOrden.id_producto = Taller..Producto.id
				WHERE Orden.id = @idPedido
	END;'')
		END'
EXEC sp_MSforeachdb @PROCS;
GO

DECLARE @PROCS VARCHAR(MAX)
SELECT @PROCS = 'IF ''?'' IN (''Sucursal1'', ''Sucursal2'', ''Sucursal3'', ''Sucursal4'') BEGIN USE ? EXEC(''
	CREATE OR ALTER PROC actualizarPedido @idPedido int, @estado bit AS
	BEGIN
		UPDATE OrdenxMensajero SET entregado = @estado WHERE id_orden = @idPedido
	END;'')
	END'
EXEC sp_MSforeachdb @PROCS;
GO



DECLARE @PROCS VARCHAR(MAX)
SELECT @PROCS = 'IF ''?'' IN (''Sucursal1'', ''Sucursal2'', ''Sucursal3'', ''Sucursal4'') BEGIN USE ? EXEC(''
	CREATE OR ALTER PROC agregarCupon @factura int, @cupon int AS
	BEGIN
		IF EXISTS (SELECT 1 FROM openquery(ATENCIONCLIENTE,''''select id from AtencionCliente.Cupon'''') AS Cupones WHERE Cupones.id = @cupon)
			INSERT INTO CuponxFactura VALUES (@factura, @cupon)
		ELSE
			PRINT N''''No existe el cupon ingresado.''''
	END;'')
	END'
EXEC sp_MSforeachdb @PROCS;
GO

DECLARE @PROCS VARCHAR(MAX)
SELECT @PROCS = 'IF ''?'' IN (''Sucursal1'', ''Sucursal2'', ''Sucursal3'', ''Sucursal4'') BEGIN USE ? EXEC(''
	CREATE OR ALTER PROC agregarOferta @factura int, @oferta int AS
	BEGIN
		IF EXISTS (SELECT 1 FROM openquery(ATENCIONCLIENTE,''''select id from AtencionCliente.Oferta'''') AS Ofertas WHERE Ofertas.id = @oferta)
			INSERT INTO OfertaxFactura VALUES (@factura, @oferta)
		ELSE
			PRINT N''''No existe la oferta ingresada.''''
	END;'')
	END'
EXEC sp_MSforeachdb @PROCS;
GO

DECLARE @PROCS VARCHAR(MAX)
SELECT @PROCS = 'IF ''?'' IN (''Sucursal1'', ''Sucursal2'', ''Sucursal3'', ''Sucursal4'') BEGIN USE ? EXEC(''
	CREATE OR ALTER PROC crearOrden @idCliente int, @idSucursal int AS
	BEGIN
		INSERT INTO Orden VALUES (@idCliente, @idSucursal, GETDATE())
		SELECT * FROM Orden WHERE Orden.id = IDENT_CURRENT(''''Orden'''')
	END;'')
	END'
EXEC sp_MSforeachdb @PROCS;
GO

DECLARE @PROCS VARCHAR(MAX)
SELECT @PROCS = 'IF ''?'' IN (''Sucursal1'', ''Sucursal2'', ''Sucursal3'', ''Sucursal4'') BEGIN USE ? EXEC(''
	CREATE OR ALTER PROC pagarSalario @idEmpleado int = NULL AS
		BEGIN
			SELECT EMP.id AS id_empleado, EMP.nombre AS nombre, EMP.apellido AS apellido, EMP.cedula AS cedula, EMP.salario + (SELECT (SUM(DetalleOrden.precio) * 0.05) FROM Factura JOIN Orden ON Factura.id_orden = Orden.id JOIN DetalleOrden ON DetalleOrden.id_orden = Orden.id
					WHERE Factura.id_vendedor = EMP.id) AS salario
				FROM Empleado AS EMP
					WHERE EMP.id = ISNULL(@idEmpleado, EMP.id) AND EMP.id_puesto = 5
			UNION
			SELECT EMP.id AS id_empleado, EMP.nombre AS nombre, EMP.apellido AS apellido, EMP.cedula AS cedula, EMP.salario AS salario
				FROM Empleado AS EMP
					WHERE EMP.id = ISNULL(@idEmpleado, EMP.id) AND EMP.id_puesto  != 5
		END;'')
	END'
EXEC sp_MSforeachdb @PROCS;
GO

DECLARE @PROCS VARCHAR(MAX)
SELECT @PROCS = 'IF ''?'' IN (''Sucursal1'', ''Sucursal2'', ''Sucursal3'', ''Sucursal4'') BEGIN USE ? EXEC(''
	CREATE OR ALTER PROC updateMetodoPago @id int, @nombre varchar(25) = NULL AS
	BEGIN
		UPDATE MetodoPago SET nombre = ISNULL(@nombre, nombre) WHERE id = @id
	END;'')
	END'
EXEC sp_MSforeachdb @PROCS;
GO

DECLARE @PROCS VARCHAR(MAX)
SELECT @PROCS = 'IF ''?'' IN (''Sucursal1'', ''Sucursal2'', ''Sucursal3'', ''Sucursal4'') BEGIN USE ? EXEC(''
	CREATE OR ALTER PROC crearMetodoPago @nombre varchar(25) AS
	BEGIN
		INSERT INTO MetodoPago VALUES (@nombre)
	END;'')
	END'
EXEC sp_MSforeachdb @PROCS;
GO

DECLARE @PROCS VARCHAR(MAX)
SELECT @PROCS = 'IF ''?'' IN (''Sucursal1'', ''Sucursal2'', ''Sucursal3'', ''Sucursal4'') BEGIN USE ? EXEC(''
	CREATE OR ALTER PROC getMetodoPago AS
	BEGIN
		SELECT * FROM MetodoPago
	END;'')
	END'
EXEC sp_MSforeachdb @PROCS;
GO

DECLARE @PROCS VARCHAR(MAX)
SELECT @PROCS = 'IF ''?'' IN (''Sucursal1'', ''Sucursal2'', ''Sucursal3'', ''Sucursal4'') BEGIN USE ? EXEC(''
	CREATE OR ALTER PROC comisiones @idVendedor int AS
		BEGIN
			SELECT (SUM(DetalleOrden.precio) * 0.05) AS comision FROM Factura JOIN Orden ON Factura.id_orden = Orden.id JOIN DetalleOrden ON DetalleOrden.id_orden = Orden.id
				WHERE Factura.id_vendedor = @idVendedor
		END;'')
	END'
EXEC sp_MSforeachdb @PROCS;
GO

DECLARE @PROCS VARCHAR(MAX)
SELECT @PROCS = 'IF ''?'' IN (''Sucursal1'', ''Sucursal2'', ''Sucursal3'', ''Sucursal4'') BEGIN USE ? EXEC(''
	CREATE OR ALTER PROC facturar @tipoPago int, @idVendedor int = NULL, @idOrden int, @idCupon int = NULL, @idOferta int = NULL AS
	DECLARE @fact int
	BEGIN
		INSERT INTO Factura VALUES (@tipoPago, ISNULL(@idVendedor, (SELECT TOP 1 id FROM Empleado WHERE Empleado.id_puesto = 5 ORDER BY NEWID())), @idOrden, GETDATE())
		SET @fact = IDENT_CURRENT(''''Factura'''')
		EXEC agregarCupon @factura = @fact, @cupon = @idCupon
		EXEC agregarOferta @factura = @fact, @oferta = @idOferta

		SELECT * FROM Factura WHERE Factura.id = IDENT_CURRENT(''''Factura'''')
	END;'')
	END'
EXEC sp_MSforeachdb @PROCS;
GO

DECLARE @PROCS VARCHAR(MAX)
SELECT @PROCS = 'IF ''?'' IN (''Sucursal1'', ''Sucursal2'', ''Sucursal3'', ''Sucursal4'') BEGIN USE ? EXEC(''
	CREATE OR ALTER PROC agregarDetalle @orden int, @prod int, @cantidad int AS
	BEGIN
		IF (SELECT cantidad FROM Inventario WHERE Inventario.id_producto = @prod) >= @cantidad
			BEGIN
				IF NOT EXISTS (SELECT 1 FROM DetalleOrden WHERE DetalleOrden.id_orden = @orden AND DetalleOrden.id_producto = @prod)
					BEGIN
						INSERT INTO DetalleOrden VALUES (@orden, @prod, @cantidad, (SELECT precio_venta FROM Taller..Producto WHERE Taller..Producto.id = @prod) * @cantidad);
						UPDATE Inventario SET cantidad = cantidad - @cantidad WHERE Inventario.id_producto = @prod
						IF (SELECT cantidad FROM Inventario WHERE Inventario.id_producto = @prod) = 0
							EXEC reducirProd @prod, 1
					END
				ELSE
					BEGIN
						UPDATE DetalleOrden SET cantidad = cantidad + @cantidad
						UPDATE DetalleOrden SET precio = precio + ((SELECT precio_venta FROM Taller..Producto WHERE Taller..Producto.id = @prod) * @cantidad)
						UPDATE Inventario SET cantidad = cantidad - @cantidad WHERE Inventario.id_producto = @prod
						IF (SELECT cantidad FROM Inventario WHERE Inventario.id_producto = @prod) = 0
							EXEC reducirProd @prod, 1
					END
			END
		ELSE
			PRINT N''''No hay suficiente cantidad de este producto.''''
	END;'')
	END'
EXEC sp_MSforeachdb @PROCS;
GO

DECLARE @PROCS VARCHAR(MAX)
SELECT @PROCS = 'IF ''?'' IN (''Sucursal1'', ''Sucursal2'', ''Sucursal3'', ''Sucursal4'') BEGIN USE ? EXEC(''
	CREATE OR ALTER PROC agregarPedidoMensajero @orden int, @mensajero int AS
	BEGIN
		INSERT INTO OrdenxMensajero VALUES (@orden, @mensajero, 0)
	END;'')
	END'
EXEC sp_MSforeachdb @PROCS;
GO

DECLARE @PROCS VARCHAR(MAX)
SELECT @PROCS = 'IF ''?'' IN (''Sucursal1'', ''Sucursal2'', ''Sucursal3'', ''Sucursal4'') BEGIN USE ? EXEC(''
	CREATE OR ALTER PROC checkCuponCliente @idCliente int AS
	BEGIN
		SELECT *
		FROM openquery(ATENCIONCLIENTE, ''''SELECT CC.id_cliente as cliente, C.id AS id_cupon,  C.descuento_porcentual AS descuento, C.descripcion AS descripcion, 
			CC.fecha_emision AS emitido, CC.fecha_vencimiento AS vencimiento, CC.gastado AS gastado
				FROM AtencionCliente.CuponxCliente AS CC JOIN AtencionCliente.Cupon AS C ON CC.id_cupon = C.id'''') WHERE @idCliente = cliente AND gastado = 0
	END;'')
	END'
EXEC sp_MSforeachdb @PROCS;
GO

DECLARE @PROCS VARCHAR(MAX)
SELECT @PROCS = 'IF ''?'' IN (''Sucursal1'', ''Sucursal2'', ''Sucursal3'', ''Sucursal4'') BEGIN USE ? EXEC(''
	CREATE OR ALTER PROC getVendedores AS
	BEGIN
		SELECT RecursosHumanos..Empleado.id AS id, RecursosHumanos..Empleado.nombre + RecursosHumanos..Empleado.apellido AS nombre 
			FROM RecursosHumanos..Empleado	WHERE RecursosHumanos..Empleado.id_puesto = 5
	END;'')
	END'
EXEC sp_MSforeachdb @PROCS;
GO

DECLARE @PROCS VARCHAR(MAX)
SELECT @PROCS = 'IF ''?'' IN (''Sucursal1'', ''Sucursal2'', ''Sucursal3'', ''Sucursal4'') BEGIN USE ? EXEC(''
	CREATE OR ALTER PROC getTotal @idFactura int AS
	DECLARE @descCupon decimal (20,2) = 0
	DECLARE @descOferta decimal (20,2) = 0
	BEGIN
		IF EXISTS (SELECT 1 FROM CuponxFactura WHERE CuponxFactura.id_factura = @idFactura)
			SET @descCupon = (SELECT cDesc
				FROM openquery(ATENCIONCLIENTE, ''''SELECT C.id AS idC,  C.descuento_porcentual AS cDesc FROM AtencionCliente.CuponxCliente AS CC JOIN AtencionCliente.Cupon AS C ON CC.id_cupon = C.id'''')
					JOIN CuponxFactura ON CuponxFactura.id_cupon = idC) * 0.01
		IF EXISTS (SELECT 1 FROM OfertaxFactura WHERE OfertaxFactura.id_factura = @idFactura)
			SET @descOferta = (SELECT oDesc
				FROM openquery(ATENCIONCLIENTE, ''''SELECT id AS idO,  descuento_porcentual AS oDesc FROM AtencionCliente.Oferta'''')
					JOIN OfertaxFactura ON OfertaxFactura.id_oferta= idO) * 0.01
		RETURN(SELECT (SUM(DetalleOrden.precio) - (SUM(DetalleOrden.precio) * @descCupon) - (SUM(DetalleOrden.precio) * @descOferta)) AS total
				FROM Factura JOIN Orden ON Factura.id_orden = Orden.id JOIN DetalleOrden ON DetalleOrden.id_orden = Orden.id
					WHERE Factura.id = @idFactura)
	END;'')
	END'
EXEC sp_MSforeachdb @PROCS;
GO

DECLARE @PROCS VARCHAR(MAX)
SELECT @PROCS = 'IF ''?'' IN (''Sucursal1'', ''Sucursal2'', ''Sucursal3'', ''Sucursal4'') BEGIN USE ? EXEC(''
	CREATE OR ALTER PROC getSubTotal @idFactura int AS
	DECLARE @descCupon decimal (20,2) = 0
	DECLARE @descOferta decimal (20,2) = 0
	BEGIN
		IF EXISTS (SELECT 1 FROM CuponxFactura WHERE CuponxFactura.id_factura = @idFactura)
			SET @descCupon = (SELECT cDesc
				FROM openquery(ATENCIONCLIENTE, ''''SELECT C.id AS idC,  C.descuento_porcentual AS cDesc FROM AtencionCliente.CuponxCliente AS CC JOIN AtencionCliente.Cupon AS C ON CC.id_cupon = C.id'''')
					JOIN CuponxFactura ON CuponxFactura.id_cupon = idC) * 0.01
		IF EXISTS (SELECT 1 FROM OfertaxFactura WHERE OfertaxFactura.id_factura = @idFactura)
			SET @descOferta = (SELECT oDesc
				FROM openquery(ATENCIONCLIENTE, ''''SELECT id AS idO,  descuento_porcentual AS oDesc FROM AtencionCliente.Oferta'''')
					JOIN OfertaxFactura ON OfertaxFactura.id_oferta= idO) * 0.01
		RETURN (SELECT SUM(DetalleOrden.precio) AS subtotal
					FROM Factura JOIN Orden ON Factura.id_orden = Orden.id JOIN DetalleOrden ON DetalleOrden.id_orden = Orden.id
						WHERE Factura.id = @idFactura)
	END;'')
	END'
EXEC sp_MSforeachdb @PROCS;
GO

DECLARE @PROCS VARCHAR(MAX)
SELECT @PROCS = 'IF ''?'' IN (''Sucursal1'', ''Sucursal2'', ''Sucursal3'', ''Sucursal4'') BEGIN USE ? EXEC(''
	CREATE OR ALTER PROC getFactura @id int AS
	DECLARE @subtotal decimal (20,2)
	DECLARE @total decimal (20,2)
	BEGIN
		EXEC @subtotal = getSubTotal @id
		EXEC @total = getTotal @id

		SELECT Factura.id AS id, Factura.id_orden AS id_orden, Factura.id_vendedor AS id_vendedor, (RecursosHumanos..Empleado.nombre + " " + RecursosHumanos..Empleado.apellido) AS nombre_vendedor,
			Cliente.id AS id_cliente, Cliente.nombre AS nombre_cliente, Factura.id_metodoPago AS id_metodo_pago, MetodoPago.nombre AS nombre_metodo_pago,Factura.fecha_compra AS fecha, @subtotal AS subtotal, @total AS total
			FROM Factura JOIN RecursosHumanos..Empleado ON Factura.id_vendedor = RecursosHumanos..Empleado.id JOIN Orden ON Factura.id_orden = Orden.id
				JOIN openquery(ATENCIONCLIENTE,''''select id AS id, CONCAT(nombre, " ", apellido) AS nombre from AtencionCliente.cliente'''') AS Cliente ON Cliente.id = Orden.id_cliente
					JOIN MetodoPago ON MetodoPago.id = Factura.id_metodoPago
				WHERE Factura.id = @id
	END;'')
	END'
EXEC sp_MSforeachdb @PROCS;
GO

DECLARE @PROCS VARCHAR(MAX)
SELECT @PROCS = 'IF ''?'' IN (''Sucursal1'', ''Sucursal2'', ''Sucursal3'', ''Sucursal4'') BEGIN USE ? EXEC(''
	CREATE OR ALTER PROC buscarOrdenXCliente @idCliente int AS
	BEGIN
		SELECT Orden.id AS id_orden, Orden.id_sucursal AS id_sucursal, RecursosHumanos..Sucursal.nombre AS nombre_sucursal, Orden.id_cliente AS id_cliente,
			Cliente.nombre AS nombre_cliente, Orden.emision AS emision
			FROM Orden JOIN RecursosHumanos..Sucursal ON RecursosHumanos..Sucursal.id = Orden.id_sucursal
				JOIN openquery(ATENCIONCLIENTE,''''select id AS id, CONCAT(nombre, " ", apellido) AS nombre from AtencionCliente.cliente'''') AS Cliente ON Cliente.id = Orden.id_cliente
				WHERE Orden.id_cliente = @idCliente
	END;'')
	END'
EXEC sp_MSforeachdb @PROCS;
GO

DECLARE @PROCS VARCHAR(MAX)
SELECT @PROCS = 'IF ''?'' IN (''Sucursal1'', ''Sucursal2'', ''Sucursal3'', ''Sucursal4'') BEGIN USE ? EXEC(''
	CREATE OR ALTER PROC buscarFacturaXCliente @idCliente int AS
	BEGIN
		SELECT Factura.id AS id_factura, Factura.id_orden AS id_orden, Factura.id_vendedor AS id_vendedor, Empleado.nombre + Empleado.apellido AS nombre_vendedor, 
			Factura.id_metodoPago AS id_metodo_pago, MetodoPago.nombre AS nombre_metodo_pago, Factura.fecha_compra AS fecha_compra
			FROM Factura JOIN Orden ON Factura.id_orden = Orden.id JOIN Empleado ON Factura.id_vendedor = Empleado.id
				JOIN MetodoPago ON MetodoPago.id = Factura.id_metodoPago
				WHERE Orden.id_cliente = @idCliente
	END;'')
	END'
EXEC sp_MSforeachdb @PROCS;
GO

DECLARE @PROCS VARCHAR(MAX)
SELECT @PROCS = 'IF ''?'' IN (''Sucursal1'', ''Sucursal2'', ''Sucursal3'', ''Sucursal4'') BEGIN USE ? EXEC(''
	CREATE OR ALTER PROC agregarEvaluacion @orden int, @evaluacion int AS
	BEGIN
		INSERT INTO EvaluacionEnvio VALUES (@orden, @evaluacion, GETDATE());
	END;'')
	END'
EXEC sp_MSforeachdb @PROCS;
GO

DECLARE @PROCS VARCHAR(MAX)
SELECT @PROCS = 'IF ''?'' IN (''Sucursal1'', ''Sucursal2'', ''Sucursal3'', ''Sucursal4'') BEGIN USE ? EXEC(''
	CREATE OR ALTER PROC verDetalleOrden @idOrden int AS
	BEGIN
		SELECT * FROM DetalleOrden WHERE DetalleOrden.id_orden = @idOrden
	END;'')
	END'
EXEC sp_MSforeachdb @PROCS;
GO

DECLARE @PROCS VARCHAR(MAX)
SELECT @PROCS = 'IF ''?'' IN (''Sucursal1'', ''Sucursal2'', ''Sucursal3'', ''Sucursal4'') BEGIN USE ? EXEC(''
	CREATE OR ALTER PROC verOrden @idOrden int AS
	BEGIN
		SELECT * FROM Orden WHERE Orden.id = @idOrden
	END;'')
	END'
EXEC sp_MSforeachdb @PROCS;
GO

DECLARE @PROCS VARCHAR(MAX)
SELECT @PROCS = 'IF ''?'' IN (''Sucursal1'', ''Sucursal2'', ''Sucursal3'', ''Sucursal4'') BEGIN USE ? EXEC(''
	CREATE OR ALTER PROC verEmpleados AS
	BEGIN
		SELECT Empleado.id AS id, Empleado.nombre AS nombre, Empleado.apellido AS apellidos, RecursosHumanos..FotoEmpleado.imagen AS foto, Empleado.cedula AS cedula,
		RecursosHumanos..Puesto.nombre AS puesto, Empleado.salario AS salario, Empleado.telefono AS telefono, Empleado.email AS correo, Empleado.fecha_contratacion AS fecha_contrato
				FROM Empleado JOIN RecursosHumanos..FotoEmpleado ON Empleado.id_foto = FotoEmpleado.id JOIN RecursosHumanos..Puesto ON Empleado.id_puesto = Puesto.id
	END;'')
	END'
EXEC sp_MSforeachdb @PROCS;
GO

DECLARE @PROCS VARCHAR(MAX)
SELECT @PROCS = 'IF ''?'' IN (''Sucursal1'', ''Sucursal2'', ''Sucursal3'', ''Sucursal4'') BEGIN USE ? EXEC(''
	CREATE OR ALTER PROC getSucursalEmpleado @idEmpleado int AS
	BEGIN
		IF EXISTS (SELECT 1 FROM Sucursal1..Empleado WHERE Sucursal1..Empleado.id = @idEmpleado)
			RETURN 0
		ELSE
			IF EXISTS (SELECT 1 FROM Sucursal2..Empleado WHERE Sucursal2..Empleado.id = @idEmpleado)
				RETURN 1
			ELSE
				IF EXISTS (SELECT 1 FROM Sucursal3..Empleado WHERE Sucursal3..Empleado.id = @idEmpleado)
					RETURN 2
				ELSE
					IF EXISTS (SELECT 1 FROM Sucursal4..Empleado WHERE Sucursal4..Empleado.id = @idEmpleado)
						RETURN 3
	END;'')
	END'
EXEC sp_MSforeachdb @PROCS;
GO

DECLARE @PROCS VARCHAR(MAX)
SELECT @PROCS = 'IF ''?'' IN (''Sucursal1'', ''Sucursal2'', ''Sucursal3'', ''Sucursal4'') BEGIN USE ? EXEC(''
	CREATE OR ALTER PROC buscarEmpleado @idEmpleado int AS
	BEGIN
		SELECT Empleado.id AS id, Empleado.nombre AS nombre, Empleado.apellido AS apellidos, RecursosHumanos..FotoEmpleado.imagen AS foto, Empleado.cedula AS cedula,
		RecursosHumanos..Puesto.nombre AS puesto, Empleado.salario AS salario, Empleado.telefono AS telefono, Empleado.email AS correo, Empleado.fecha_contratacion AS fecha_contrato
				FROM Empleado JOIN RecursosHumanos..FotoEmpleado ON Empleado.id_foto = FotoEmpleado.id JOIN RecursosHumanos..Puesto ON Empleado.id_puesto = Puesto.id
			WHERE Empleado.id = @idEmpleado
	END;'')
	END'
EXEC sp_MSforeachdb @PROCS;
GO

USE master
GO

CREATE OR ALTER PROC sp_login @email varchar(25), @pass varchar(25) AS
	DECLARE @tipoUsuario varchar(20)
	DECLARE @idSucursal int
BEGIN
	IF EXISTS (SELECT 1 FROM openquery(ATENCIONCLIENTE,'SELECT email AS correo, password AS pass FROM AtencionCliente.Cliente') AS Clientes WHERE Clientes.correo = @email AND Clientes.pass = @pass)
		BEGIN
			SET @tipoUsuario = 'Cliente'
			SELECT @tipoUsuario AS tipo_usuario, Clientes.idCliente AS id_cliente, Clientes.latitud AS latitud, Clientes.longitud AS longitud, Clientes.nombre AS nombre, Clientes.apellido AS apellido, Clientes.fecha_nacimiento AS fecha_nacimiento, Clientes.correo AS correo
				FROM openquery(ATENCIONCLIENTE,'SELECT id AS idCliente, ST_X(ubicacion) AS latitud, ST_Y(ubicacion) AS longitud, nombre, apellido, fecha_nacimiento, email AS correo, password AS pass FROM AtencionCliente.Cliente') AS Clientes 
				WHERE Clientes.correo = @email AND Clientes.pass = @pass
		END
	ELSE
		IF EXISTS (SELECT 1 FROM Sucursal1..Empleado AS EMPLEADOS WHERE EMPLEADOS.email = @email AND EMPLEADOS.contrasena = @pass)
			SET @idSucursal = 0
		ELSE
			IF EXISTS (SELECT 1 FROM Sucursal2..Empleado AS EMPLEADOS WHERE EMPLEADOS.email = @email AND EMPLEADOS.contrasena = @pass)
				SET @idSucursal = 1
			ELSE
				IF EXISTS (SELECT 1 FROM Sucursal3..Empleado AS EMPLEADOS WHERE EMPLEADOS.email = @email AND EMPLEADOS.contrasena = @pass)
					SET @idSucursal = 2
				ELSE
					IF EXISTS (SELECT 1 FROM Sucursal4..Empleado AS EMPLEADOS WHERE EMPLEADOS.email = @email AND EMPLEADOS.contrasena = @pass)
						SET @idSucursal = 3
		IF EXISTS (SELECT 1 FROM RecursosHumanos..Empleado AS EMPLEADOS WHERE EMPLEADOS.email = @email AND EMPLEADOS.contrasena = @pass)
			BEGIN
				SET @tipoUsuario = 'Empleado'
				SELECT @tipoUsuario AS tipo_usuario, @idSucursal AS id_sucursal, EMPLEADOS.id, FOTOS.imagen, EMPLEADOS.id_puesto, EMPLEADOS.nombre, EMPLEADOS.apellido,
					EMPLEADOS.salario, EMPLEADOS.telefono, EMPLEADOS.email, EMPLEADOS.fecha_contratacion, EMPLEADOS.cedula
					FROM RecursosHumanos..Empleado AS EMPLEADOS JOIN RecursosHumanos..FotoEmpleado AS FOTOS ON FOTOS.id = EMPLEADOS.id_foto 
						WHERE EMPLEADOS.email = @email AND EMPLEADOS.contrasena = @pass
			END
END;
GO

CREATE OR ALTER PROCEDURE sp_ventas
	@sucursal int = NULL,
	@prod int = NULL,
	@vendedor int = NULL,
	@fechaI date = NULL,
	@fechaF date = NULL
AS

BEGIN

	CREATE TABLE #datosSucursales(
		idSucursal int,
		idProducto int,
		idVendedor int,
		fecha date,
		cantidad int
	);

	INSERT INTO #datosSucursales(idSucursal, idProducto, idVendedor, fecha, cantidad)
		(SELECT Sucursal1..Orden.id_sucursal, Sucursal1..DetalleOrden.id_producto,Sucursal1..Factura.id_vendedor, Sucursal1..Factura.fecha_compra, Sucursal1..DetalleOrden.cantidad
			FROM Sucursal1..Factura JOIN Sucursal1..Orden ON Sucursal1..Factura.id_orden = Sucursal1..Orden.id
				JOIN Sucursal1..DetalleOrden ON Sucursal1..DetalleOrden.id_orden = Sucursal1..Orden.id
		UNION
		SELECT Sucursal2..Orden.id_sucursal, Sucursal2..DetalleOrden.id_producto,Sucursal2..Factura.id_vendedor, Sucursal2..Factura.fecha_compra, Sucursal2..DetalleOrden.cantidad
			FROM Sucursal2..Factura JOIN Sucursal2..Orden ON Sucursal2..Factura.id_orden = Sucursal2..Orden.id
				JOIN Sucursal2..DetalleOrden ON Sucursal2..DetalleOrden.id_orden = Sucursal2..Orden.id
		UNION
		SELECT Sucursal3..Orden.id_sucursal, Sucursal3..DetalleOrden.id_producto,Sucursal3..Factura.id_vendedor, Sucursal3..Factura.fecha_compra, Sucursal3..DetalleOrden.cantidad
			FROM Sucursal3..Factura JOIN Sucursal3..Orden ON Sucursal3..Factura.id_orden = Sucursal3..Orden.id
				JOIN Sucursal3..DetalleOrden ON Sucursal3..DetalleOrden.id_orden = Sucursal3..Orden.id
		UNION
		SELECT Sucursal4..Orden.id_sucursal, Sucursal4..DetalleOrden.id_producto,Sucursal4..Factura.id_vendedor, Sucursal4..Factura.fecha_compra, Sucursal4..DetalleOrden.cantidad
			FROM Sucursal4..Factura JOIN Sucursal4..Orden ON Sucursal4..Factura.id_orden = Sucursal4..Orden.id
				JOIN Sucursal4..DetalleOrden ON Sucursal4..DetalleOrden.id_orden = Sucursal4..Orden.id
		);

	SELECT
		CASE WHEN ((GROUPING(RHS.nombre) = 1)) THEN 'ALL'
			ELSE ISNULL(RHS.nombre, 'UNKNOWN')
		END AS sucursal,
		CASE WHEN ((GROUPING(Prod.nombre) = 1)) THEN 'ALL'
			ELSE ISNULL(Prod.nombre, 'UNKNOWN')
		END AS producto, 
		CASE WHEN ((GROUPING(RHE.nombre) = 1)) THEN 'ALL'
			ELSE ISNULL(RHE.nombre, 'UNKNOWN')
		END AS vendedor,
		SUM(Prod.precio_venta * #DS.cantidad) AS venta,
		COUNT(PROD.id) AS cantidad
		FROM RecursosHumanos..Sucursal AS RHS JOIN #datosSucursales AS #DS ON RHS.id = #DS.idSucursal JOIN Taller..Producto AS Prod ON Prod.id = #DS.idProducto
				JOIN RecursosHumanos..Empleado AS RHE ON RHE.id = #DS.idVendedor
		WHERE ((#DS.fecha >= @fechaI AND #DS.fecha <= @fechaF) OR (@fechaI IS NULL AND @fechaF IS NULL))  
				AND (RHS.id = ISNULL(@sucursal, RHS.id)) AND (Prod.id = ISNULL(@prod, Prod.id))
					AND (RHE.id = ISNULL(@vendedor, RHE.id))
		GROUP BY RHS.nombre, Prod.nombre, RHE.nombre, #DS.fecha WITH ROLLUP
END;
GO

CREATE OR ALTER PROCEDURE sp_ganancias
	@sucursal int = NULL,
	@prod int = NULL,
	@vendedor int = NULL,
	@fechaI date = NULL,
	@fechaF date = NULL
AS

BEGIN
	CREATE TABLE #datosSucursales(
		idSucursal int,
		idProducto int,
		idVendedor int,
		fecha date,
		cantidad int
	);

	INSERT INTO #datosSucursales(idSucursal, idProducto, idVendedor, fecha, cantidad)
		(SELECT Sucursal1..Orden.id_sucursal, Sucursal1..DetalleOrden.id_producto,Sucursal1..Factura.id_vendedor, Sucursal1..Factura.fecha_compra, Sucursal1..DetalleOrden.cantidad
			FROM Sucursal1..Factura JOIN Sucursal1..Orden ON Sucursal1..Factura.id_orden = Sucursal1..Orden.id
				JOIN Sucursal1..DetalleOrden ON Sucursal1..DetalleOrden.id_orden = Sucursal1..Orden.id
		UNION
		SELECT Sucursal2..Orden.id_sucursal, Sucursal2..DetalleOrden.id_producto,Sucursal2..Factura.id_vendedor, Sucursal2..Factura.fecha_compra, Sucursal2..DetalleOrden.cantidad
			FROM Sucursal2..Factura JOIN Sucursal2..Orden ON Sucursal2..Factura.id_orden = Sucursal2..Orden.id
				JOIN Sucursal2..DetalleOrden ON Sucursal2..DetalleOrden.id_orden = Sucursal2..Orden.id
		UNION
		SELECT Sucursal3..Orden.id_sucursal, Sucursal3..DetalleOrden.id_producto,Sucursal3..Factura.id_vendedor, Sucursal3..Factura.fecha_compra, Sucursal3..DetalleOrden.cantidad
			FROM Sucursal3..Factura JOIN Sucursal3..Orden ON Sucursal3..Factura.id_orden = Sucursal3..Orden.id
				JOIN Sucursal3..DetalleOrden ON Sucursal3..DetalleOrden.id_orden = Sucursal3..Orden.id
		UNION
		SELECT Sucursal4..Orden.id_sucursal, Sucursal4..DetalleOrden.id_producto,Sucursal4..Factura.id_vendedor, Sucursal4..Factura.fecha_compra, Sucursal4..DetalleOrden.cantidad
			FROM Sucursal4..Factura JOIN Sucursal4..Orden ON Sucursal4..Factura.id_orden = Sucursal4..Orden.id
				JOIN Sucursal4..DetalleOrden ON Sucursal4..DetalleOrden.id_orden = Sucursal4..Orden.id
		);

	SELECT
		CASE WHEN ((GROUPING(RHS.nombre) = 1)) THEN 'ALL'
			ELSE ISNULL(RHS.nombre, 'UNKNOWN')
		END AS sucursal,
		CASE WHEN ((GROUPING(Prod.nombre) = 1)) THEN 'ALL'
			ELSE ISNULL(Prod.nombre, 'UNKNOWN')
		END AS producto, 
		CASE WHEN ((GROUPING(RHE.nombre) = 1)) THEN 'ALL'
			ELSE ISNULL(RHE.nombre, 'UNKNOWN')
		END AS vendedor,
		SUM((Prod.precio_venta - Prod.precio_costo) * #DS.cantidad) AS ganancias
		FROM RecursosHumanos..Sucursal AS RHS JOIN #datosSucursales AS #DS ON RHS.id = #DS.idSucursal JOIN Taller..Producto AS Prod ON Prod.id = #DS.idProducto
				JOIN RecursosHumanos..Empleado AS RHE ON RHE.id = #DS.idVendedor
		WHERE ((#DS.fecha >= @fechaI AND #DS.fecha <= @fechaF) OR (@fechaI IS NULL AND @fechaF IS NULL)) 
				AND (RHS.id = ISNULL(@sucursal, RHS.id)) AND (Prod.id = ISNULL(@prod, Prod.id))
					AND (RHE.id = ISNULL(@vendedor, RHE.id))
		GROUP BY RHS.nombre, Prod.nombre, RHE.nombre, #DS.fecha WITH ROLLUP
END;
GO

EXEC sp_ms_marksystemobject sp_login
EXEC sp_ms_marksystemobject sp_ventas
EXEC sp_ms_marksystemobject sp_ganancias