CREATE DATABASE [RecursosHumanos];
GO

USE [RecursosHumanos];

CREATE TABLE [Puesto] (
  [id] int IDENTITY(0, 1) NOT NULL,
  [nombre] varchar(25) NOT NULL,
  [salario_base] decimal (20,2) NOT NULL,

  PRIMARY KEY ([id])
);

CREATE TABLE [FotoEmpleado] (
  [id] int IDENTITY(0, 1) NOT NULL,
  [imagen] varchar(max) NOT NULL,
  
  PRIMARY KEY ([id])
);

CREATE TABLE [Empleado] (
  [id] int IDENTITY(0, 1) NOT NULL,
  [id_foto] int NOT NULL,
  [id_puesto] int NOT NULL,
  [nombre] varchar(25) NOT NULL,
  [apellido] varchar(50) NOT NULL,
  [salario] decimal (20,2) NOT NULL,
  [telefono] varchar(8) NOT NULL,
  [email] varchar(25) UNIQUE NOT NULL,
  [contrasena] varchar(25) NOT NULL,
  [fecha_contratacion] datetime NOT NULL,
  [cedula] varchar(12) UNIQUE NOT NULL,

  PRIMARY KEY ([id]),
  FOREIGN KEY ([id_foto]) REFERENCES [FotoEmpleado]([id]),
  FOREIGN KEY ([id_puesto]) REFERENCES [Puesto]([id])
);

--CREATE INDEX [FK] ON  [Empleado] ([id_foto], [id_puesto]);

CREATE TABLE [Sucursal] (
  [id] int IDENTITY(0, 1) NOT NULL,
  [nombre] varchar(25) NOT NULL,
  [ubicacion] geography NOT NULL,
  [nombre_base] varchar(20) NOT NULL,

  PRIMARY KEY ([id])
);