USE RecursosHumanos
GO

CREATE OR ALTER FUNCTION getSucursalEmpleado(@idEmpleado int) RETURNS INT AS
BEGIN
	IF EXISTS (SELECT 1 FROM Sucursal1..Empleado WHERE Sucursal1..Empleado.id = @idEmpleado)
		RETURN 0
	ELSE
		IF EXISTS (SELECT 1 FROM Sucursal2..Empleado WHERE Sucursal2..Empleado.id = @idEmpleado)
			RETURN 1
		ELSE
			IF EXISTS (SELECT 1 FROM Sucursal3..Empleado WHERE Sucursal3..Empleado.id = @idEmpleado)
				RETURN 2
			ELSE
				IF EXISTS (SELECT 1 FROM Sucursal4..Empleado WHERE Sucursal4..Empleado.id = @idEmpleado)
					RETURN 3
	RETURN null;
END;
GO

CREATE OR ALTER PROC buscarEmpleado @idEmpleado int = NULL, @idPuesto int = NULL AS
BEGIN
	SELECT Empleado.id AS id, Empleado.nombre AS nombre, Empleado.apellido AS apellido, FotoEmpleado.imagen AS foto, Empleado.cedula AS cedula, [dbo].[getSucursalEmpleado](Empleado.id) as id_sucursal,
		Puesto.id AS id_puesto, Puesto.nombre AS puesto, Empleado.salario AS salario, Empleado.telefono AS telefono, Empleado.email AS email, Empleado.fecha_contratacion AS fecha_contratacion
			FROM Empleado JOIN FotoEmpleado ON Empleado.id_foto = FotoEmpleado.id JOIN Puesto ON Empleado.id_puesto = Puesto.id
		WHERE Empleado.id = ISNULL(@idEmpleado, Empleado.id) AND Empleado.id_puesto = ISNULL(@idPuesto, Empleado.id_puesto)
END;
GO

CREATE OR ALTER PROC verSucursales AS
BEGIN
	SELECT * FROM Sucursal
END;
GO

CREATE OR ALTER PROC updatePuesto @id int, @nombre varchar(25) = NULL, @salario_base decimal (20,2) = NULL AS
BEGIN
	UPDATE RecursosHumanos..Puesto SET RecursosHumanos..Puesto.nombre = ISNULL(@nombre, RecursosHumanos..Puesto.nombre) WHERE RecursosHumanos..Puesto.id = @id
	UPDATE RecursosHumanos..Puesto SET RecursosHumanos..Puesto.salario_base = ISNULL(@salario_base, RecursosHumanos..Puesto.salario_base) WHERE RecursosHumanos..Puesto.id = @id
	IF NOT (@salario_base IS NULL)
		UPDATE RecursosHumanos..Empleado SET RecursosHumanos..Empleado.salario = @salario_base WHERE RecursosHumanos..Empleado.id_puesto = @id
END;
GO

CREATE OR ALTER PROC agregarEmpleadoSucursal @foto varchar(max), @puesto int, @nombre varchar(25), @apellido varchar(50), @telefono varchar(8),
		@correo varchar(25), @pass varchar(25), @cedula varchar(12), @sucursal int AS
DECLARE @salario decimal (20,2)
BEGIN

	SET @salario = (SELECT RecursosHumanos..Puesto.salario_base FROM RecursosHumanos..Puesto WHERE RecursosHumanos..Puesto.id = @puesto)
	INSERT INTO RecursosHumanos..FotoEmpleado VALUES (@foto)
	INSERT INTO RecursosHumanos..Empleado VALUES (IDENT_CURRENT('RecursosHumanos..FotoEmpleado'), @puesto, @nombre, @apellido, @salario, @telefono, @correo, @pass, GETDATE(), @cedula)
	IF @sucursal = 0
		INSERT INTO Sucursal1..Empleado VALUES (IDENT_CURRENT('RecursosHumanos..Empleado'), IDENT_CURRENT('RecursosHumanos..FotoEmpleado'), @puesto, @nombre, @apellido, @salario, @telefono, @correo, @pass, GETDATE(), @cedula)
	ELSE
		IF @sucursal = 1
			INSERT INTO Sucursal2..Empleado VALUES (IDENT_CURRENT('RecursosHumanos..Empleado'), IDENT_CURRENT('RecursosHumanos..FotoEmpleado'), @puesto, @nombre, @apellido, @salario, @telefono, @correo, @pass, GETDATE(), @cedula)
		ELSE
			IF @sucursal = 2
				INSERT INTO Sucursal3..Empleado VALUES (IDENT_CURRENT('RecursosHumanos..Empleado'), IDENT_CURRENT('RecursosHumanos..FotoEmpleado'), @puesto, @nombre, @apellido, @salario, @telefono, @correo, @pass, GETDATE(), @cedula)
			ELSE
				IF @sucursal = 3
					INSERT INTO Sucursal4..Empleado VALUES (IDENT_CURRENT('RecursosHumanos..Empleado'), IDENT_CURRENT('RecursosHumanos..FotoEmpleado'), @puesto, @nombre, @apellido, @salario, @telefono, @correo, @pass, GETDATE(), @cedula)
END;
GO

CREATE OR ALTER PROC updateEmpleado @id int, @foto varchar(max) = NULL, @puesto int = NULL, @salario decimal (20,2) = NULL, @telefono varchar(8) = NULL, 
		@correo varchar(25) = NULL, @pass varchar(25) = NULL, @nombre varchar(25) = NULL, @apellido varchar(50) = NULL, @cedula varchar(12) = NULL AS
BEGIN
		UPDATE RecursosHumanos..FotoEmpleado SET RecursosHumanos..FotoEmpleado.imagen = ISNULL(@foto, RecursosHumanos..FotoEmpleado.imagen) 
			WHERE (SELECT RecursosHumanos..Empleado.id_foto FROM RecursosHumanos..Empleado WHERE RecursosHumanos..Empleado.id = @id) = RecursosHumanos..FotoEmpleado.id;
		UPDATE RecursosHumanos..Empleado 
		SET
			RecursosHumanos..Empleado.id_puesto = ISNULL(@puesto, RecursosHumanos..Empleado.id_puesto), RecursosHumanos..Empleado.nombre = ISNULL(@nombre, RecursosHumanos..Empleado.nombre),
			RecursosHumanos..Empleado.apellido = ISNULL(@apellido, RecursosHumanos..Empleado.apellido), RecursosHumanos..Empleado.cedula = ISNULL(@cedula, RecursosHumanos..Empleado.cedula), 
			RecursosHumanos..Empleado.salario = ISNULL(@salario, RecursosHumanos..Empleado.salario), RecursosHumanos..Empleado.telefono = ISNULL(@telefono, RecursosHumanos..Empleado.telefono), 
			RecursosHumanos..Empleado.email = ISNULL(@correo, RecursosHumanos..Empleado.email), RecursosHumanos..Empleado.contrasena = ISNULL(@pass, RecursosHumanos..Empleado.contrasena)
		WHERE RecursosHumanos..Empleado.id = @id

		UPDATE Sucursal1..Empleado
		SET
			Sucursal1..Empleado.id_puesto = ISNULL(@puesto, Sucursal1..Empleado.id_puesto), Sucursal1..Empleado.nombre = ISNULL(@nombre, Sucursal1..Empleado.nombre),
			Sucursal1..Empleado.apellido = ISNULL(@apellido, Sucursal1..Empleado.apellido), Sucursal1..Empleado.cedula = ISNULL(@cedula, Sucursal1..Empleado.cedula), 
			Sucursal1..Empleado.salario = ISNULL(@salario, Sucursal1..Empleado.salario), Sucursal1..Empleado.telefono = ISNULL(@telefono, Sucursal1..Empleado.telefono), 
			Sucursal1..Empleado.email = ISNULL(@correo, Sucursal1..Empleado.email), Sucursal1..Empleado.contrasena = ISNULL(@pass, Sucursal1..Empleado.contrasena)
		WHERE Sucursal1..Empleado.id = @id

		UPDATE Sucursal2..Empleado
		SET
			Sucursal2..Empleado.id_puesto = ISNULL(@puesto, Sucursal2..Empleado.id_puesto), Sucursal2..Empleado.nombre = ISNULL(@nombre, Sucursal2..Empleado.nombre),
			Sucursal2..Empleado.apellido = ISNULL(@apellido, Sucursal2..Empleado.apellido), Sucursal2..Empleado.cedula = ISNULL(@cedula, Sucursal2..Empleado.cedula), 
			Sucursal2..Empleado.salario = ISNULL(@salario, Sucursal2..Empleado.salario), Sucursal2..Empleado.telefono = ISNULL(@telefono, Sucursal2..Empleado.telefono), 
			Sucursal2..Empleado.email = ISNULL(@correo, Sucursal2..Empleado.email), Sucursal2..Empleado.contrasena = ISNULL(@pass, Sucursal2..Empleado.contrasena)
		WHERE Sucursal2..Empleado.id = @id

		UPDATE Sucursal3..Empleado
		SET
			Sucursal3..Empleado.id_puesto = ISNULL(@puesto, Sucursal3..Empleado.id_puesto), Sucursal3..Empleado.nombre = ISNULL(@nombre, Sucursal3..Empleado.nombre),
			Sucursal3..Empleado.apellido = ISNULL(@apellido, Sucursal3..Empleado.apellido), Sucursal3..Empleado.cedula = ISNULL(@cedula, Sucursal3..Empleado.cedula), 
			Sucursal3..Empleado.salario = ISNULL(@salario, Sucursal3..Empleado.salario), Sucursal3..Empleado.telefono = ISNULL(@telefono, Sucursal3..Empleado.telefono), 
			Sucursal3..Empleado.email = ISNULL(@correo, Sucursal3..Empleado.email), Sucursal3..Empleado.contrasena = ISNULL(@pass, Sucursal3..Empleado.contrasena)
		WHERE Sucursal3..Empleado.id = @id

		UPDATE Sucursal4..Empleado
		SET
			Sucursal4..Empleado.id_puesto = ISNULL(@puesto, Sucursal4..Empleado.id_puesto), Sucursal4..Empleado.nombre = ISNULL(@nombre, Sucursal4..Empleado.nombre),
			Sucursal4..Empleado.apellido = ISNULL(@apellido, Sucursal4..Empleado.apellido), Sucursal4..Empleado.cedula = ISNULL(@cedula, Sucursal4..Empleado.cedula), 
			Sucursal4..Empleado.salario = ISNULL(@salario, Sucursal4..Empleado.salario), Sucursal4..Empleado.telefono = ISNULL(@telefono, Sucursal4..Empleado.telefono), 
			Sucursal4..Empleado.email = ISNULL(@correo, Sucursal4..Empleado.email), Sucursal4..Empleado.contrasena = ISNULL(@pass, Sucursal4..Empleado.contrasena)
		WHERE Sucursal4..Empleado.id = @id

		UPDATE Taller..Empleado
		SET
			Taller..Empleado.id_puesto = ISNULL(@puesto, Taller..Empleado.id_puesto), Taller..Empleado.nombre = ISNULL(@nombre, Taller..Empleado.nombre),
			Taller..Empleado.apellido = ISNULL(@apellido, Taller..Empleado.apellido), Taller..Empleado.cedula = ISNULL(@cedula, Taller..Empleado.cedula), 
			Taller..Empleado.salario = ISNULL(@salario, Taller..Empleado.salario), Taller..Empleado.telefono = ISNULL(@telefono, Taller..Empleado.telefono), 
			Taller..Empleado.email = ISNULL(@correo, Taller..Empleado.email), Taller..Empleado.contrasena = ISNULL(@pass, Taller..Empleado.contrasena)
		WHERE Taller..Empleado.id = @id
END;
GO

CREATE OR ALTER PROC crearPuesto @nombre varchar(25), @salario decimal (20,2) AS
BEGIN
	INSERT INTO RecursosHumanos..Puesto VALUES (@nombre, @salario)
END;
GO

CREATE OR ALTER PROC buscar_sucursal_cercana @idCliente int AS
	DECLARE @lat int
	DECLARE @long int
	DECLARE @direccion geography
	DECLARE @d1 float
	DECLARE @d2 float
	DECLARE @d3 float
	DECLARE @d4 float
BEGIN
	SET @lat = (SELECT Clientes.latitud
		FROM openquery(ATENCIONCLIENTE,'SELECT id AS id, ST_X(ubicacion) AS latitud FROM AtencionCliente.Cliente') AS Clientes
			WHERE Clientes.id = @idCliente)
	SET @long = (SELECT Clientes.longitud
		FROM openquery(ATENCIONCLIENTE,'SELECT id AS id, ST_Y(ubicacion) AS longitud FROM AtencionCliente.Cliente') AS Clientes
			WHERE Clientes.id = @idCliente)
	SET @direccion = geography::Point(@lat, @long, 4326)
	
	SELECT TOP 1 * FROM RecursosHumanos..Sucursal ORDER BY @direccion.STDistance(RecursosHumanos..Sucursal.ubicacion) ASC

END;
GO